package data;

import java.util.LinkedList;

public class VerwendungszweckListe {

	public static final String[] SPALTENNAMEN = { "Verwendungszweck" };
	private LinkedList<Verwendungszweck> verwendungszweckListe;

	public VerwendungszweckListe() {
		this.verwendungszweckListe = new LinkedList<Verwendungszweck>();
	}

	public void addVerwendungszweckListe(Verwendungszweck v) {
		this.verwendungszweckListe.add(v);
	}

	public LinkedList<Verwendungszweck> getVerwendungszweckliste() {
		return verwendungszweckListe;
	}

	public void setVerwendungszweckListe(LinkedList<Verwendungszweck> verwendungszweckListe) {
		this.verwendungszweckListe = verwendungszweckListe;
	}

	public String[] getVerwendungszweckListeV() {
		String[] tabledata0 = new String[verwendungszweckListe.size()];
		int n=0;
		for (int i = 0; i < verwendungszweckListe.size(); i++) {
			if (verwendungszweckListe.get(i).getVerwendungszweck().equals("Deine Gruppe belohnte dich f�r deine Leistungen.")==false && verwendungszweckListe.get(i).getVerwendungszweck().equals("Willkommensgeschenk!!!")==false && verwendungszweckListe.get(i).getVerwendungszweck().equals("$Gl�cksspiel$")==false && verwendungszweckListe.get(i).getVerwendungszweck().equals("")==false) {
			tabledata0[i-n] = verwendungszweckListe.get(i).getVerwendungszweck();
			}else {
				n++;
				tabledata0[verwendungszweckListe.size()-n] = verwendungszweckListe.get(i).getVerwendungszweck();
			}
		}
		String[] tabledata = new String[verwendungszweckListe.size()-n];
		for (int i = 0; i < verwendungszweckListe.size()-n; i++) {
			tabledata[i] = tabledata0[i];
		}
		return tabledata;
	}
	
	public String[][] getVerwendungszweckListe() {
		String[][] tabledata0 = new String[verwendungszweckListe.size()][SPALTENNAMEN.length];
		int n=0;
		for (int i = 0; i < verwendungszweckListe.size(); i++) {
			if (verwendungszweckListe.get(i).getVerwendungszweck().equals("Deine Gruppe belohnte dich f�r deine Leistungen.")==false && verwendungszweckListe.get(i).getVerwendungszweck().equals("Willkommensgeschenk!!!")==false && verwendungszweckListe.get(i).getVerwendungszweck().equals("$Gl�cksspiel$")==false && verwendungszweckListe.get(i).getVerwendungszweck().equals("")==false) {
			tabledata0[i-n][0] = verwendungszweckListe.get(i).getVerwendungszweck();
			}else {
				n++;
				tabledata0[verwendungszweckListe.size()-n][0] = verwendungszweckListe.get(i).getVerwendungszweck();
			}
		}
		String[][] tabledata = new String[verwendungszweckListe.size()-n][SPALTENNAMEN.length];
		for (int i = 0; i < verwendungszweckListe.size()-n; i++) {
			tabledata[i][0] = tabledata0[i][0];
		}
		return tabledata;
	}
}