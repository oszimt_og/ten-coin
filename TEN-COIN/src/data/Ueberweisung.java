package data;

public class Ueberweisung {

	private String verwendungszweck;
	private int betrag;
	private String datum;

	public Ueberweisung() {
		super();
	}

	public Ueberweisung(String verwendungszweck, int betrag, String datum) {
		super();
		this.verwendungszweck = verwendungszweck;
		this.betrag = betrag;
		this.datum = datum;
	}

	public String getVerwendungszweck() {
		return verwendungszweck;
	}

	public void setVerwendungszweck(String verwendungszweck) {
		this.verwendungszweck = verwendungszweck;
	}

	public int getBetrag() {
		return betrag;
	}

	public void setBetrag(int betrag) {
		this.betrag = betrag;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	@Override
	public String toString() {
		return "Ueberweisung [verwendungszweck=" + verwendungszweck + ", betrag=" + betrag + ", datum=" + datum + "]";
	}

}
