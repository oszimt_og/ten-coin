package data;

import java.util.LinkedList;

import database.Database;

public class SchuelerListe {

	public static final String[] SPALTENNAMEN = { "Email", "Vorname", "Name", "Kontostand" };
	private LinkedList<Benutzer> schuelerListe;
	private Database db = new Database();

	public SchuelerListe() {
		this.schuelerListe = new LinkedList<Benutzer>();
	}

	public void addSchuelerListe(Benutzer b) {
		this.schuelerListe.add(b);
	}

	public LinkedList<Benutzer> getSchuelerliste() {
		return schuelerListe;
	}

	public void setSchuelerListe(LinkedList<Benutzer> schuelerListe) {
		this.schuelerListe = schuelerListe;
	}

	public String[] getSchuelerListeV() {
		String[] tabledata = new String[schuelerListe.size()];
		for (int i = 0; i < schuelerListe.size(); i++) {
			tabledata[i] = schuelerListe.get(i).getEmail() + "|" + schuelerListe.get(i).getVorname() + "|" +  schuelerListe.get(i).getName();
		}
		return tabledata;
	}
	
	public String[][] getSchuelerListe() {
		String[][] tabledata = new String[schuelerListe.size()][SPALTENNAMEN.length];
		for (int i = 0; i < schuelerListe.size(); i++) {
			tabledata[i][0] = schuelerListe.get(i).getEmail();
			tabledata[i][1] = schuelerListe.get(i).getVorname();
			tabledata[i][2] = schuelerListe.get(i).getName();
			tabledata[i][3] = String.valueOf(db.getKontostand(schuelerListe.get(i).getEmail())) + " IT-$";
		}
		return tabledata;
	}
}