package data;

import java.util.LinkedList;

public class KlassenListe {

	private LinkedList<Klasse> klassenListe;

	public KlassenListe() {
		this.klassenListe = new LinkedList<Klasse>();
	}

	public void addKlassenListe(Klasse k) {
		this.klassenListe.add(k);
	}

	public LinkedList<Klasse> getKlassenliste() {
		return klassenListe;
	}

	public void setKlassenListe(LinkedList<Klasse> klassenListe) {
		this.klassenListe = klassenListe;
	}

	public String[] getKlassenListe() {
		String[] tabledata = new String[klassenListe.size()];
		for (int i = 0; i < klassenListe.size(); i++) {
			tabledata[i] = klassenListe.get(i).getKlasse();
		}
		return tabledata;
	}
}