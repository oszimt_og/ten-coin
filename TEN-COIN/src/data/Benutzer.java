package data;

public class Benutzer {

	private String email;
	private String vorname;
	private String name;
	private String passwort;
	private String code;

	public Benutzer() {
		super();
	}

	public Benutzer(String email, String vorname, String name, String passwort, String code) {
		super();
		this.email = email;
		this.vorname = vorname;
		this.name = name;
		this.passwort = passwort;
		this.code = code;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "Benutzer [email=" + email + ", vorname=" + vorname + ", name=" + name + ", passwort=" + passwort
				+ ", code=" + code + "]";
	}

}