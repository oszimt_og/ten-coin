package data;

import java.util.LinkedList;

public class UeberweisungsListe {

	public static final String[] SPALTENNAMEN = { "Wof�r?", "Wie viel?", "Wann?" };
	private LinkedList<Ueberweisung> ueberweisungsListe;

	public UeberweisungsListe() {
		this.ueberweisungsListe = new LinkedList<Ueberweisung>();
	}

	public void addUeberweisungsListe(Ueberweisung u) {
		this.ueberweisungsListe.add(u);
	}

	public LinkedList<Ueberweisung> getUeberweisungsliste() {
		return ueberweisungsListe;
	}

	public void setUeberweisungsListe(LinkedList<Ueberweisung> ueberweisungsListe) {
		this.ueberweisungsListe = ueberweisungsListe;
	}

	public String[][] getUeberweisungsListe() {
		String[][] tabledata = new String[ueberweisungsListe.size()][SPALTENNAMEN.length];
		for (int i = 0; i < ueberweisungsListe.size(); i++) {
			tabledata[i][0] = ueberweisungsListe.get(i).getVerwendungszweck();
			tabledata[i][1] = String.valueOf(ueberweisungsListe.get(i).getBetrag()) + " IT-$";
			tabledata[i][2] = " " + ueberweisungsListe.get(i).getDatum();
		}
		return tabledata;
	}
}