package data;

public class Verwendungszweck {

	private String verwendungszweck;

	public Verwendungszweck() {
		super();
	}

	public Verwendungszweck(String verwendungszweck) {
		super();
		this.verwendungszweck = verwendungszweck;
	}

	public String getVerwendungszweck() {
		return verwendungszweck;
	}

	public void setVerwendungszweck(String verwendungszweck) {
		this.verwendungszweck = verwendungszweck;
	}

	@Override
	public String toString() {
		return "Ueberweisung [verwendungszweck=" + verwendungszweck + "]";
	}

}
