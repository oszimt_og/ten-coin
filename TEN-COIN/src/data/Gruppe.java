package data;

public class Gruppe {

	private String gruppe;

	public Gruppe() {
		super();
	}

	public Gruppe(String gruppe) {
		super();
		this.gruppe = gruppe;
	}

	public String getGruppe() {
		return gruppe;
	}

	public void setGruppe(String gruppe) {
		this.gruppe = gruppe;
	}

	@Override
	public String toString() {
		return "Gruppe [gruppe=" + gruppe + "]";
	}

}