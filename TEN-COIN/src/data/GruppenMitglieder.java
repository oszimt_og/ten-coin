package data;

import java.util.LinkedList;

public class GruppenMitglieder {

	public static final String[] SPALTENNAMEN = { "Vorname", "Name" };
	private LinkedList<Benutzer> gruppenMitglieder;

	public GruppenMitglieder() {
		this.gruppenMitglieder = new LinkedList<Benutzer>();
	}

	public void addGruppenMitglieder(Benutzer b) {
		this.gruppenMitglieder.add(b);
	}

	public LinkedList<Benutzer> getGruppenmitglieder() {
		return gruppenMitglieder;
	}

	public void setGruppenMitglieder(LinkedList<Benutzer> gruppenMitglieder) {
		this.gruppenMitglieder = gruppenMitglieder;
	}

	public String[] getGruppenMitgliederV() {
		String[] tabledata = new String[gruppenMitglieder.size()];
		for (int i = 0; i < gruppenMitglieder.size(); i++) {
			tabledata[i] = gruppenMitglieder.get(i).getEmail();
		}
		return tabledata;
	}
	
	public String[][] getGruppenMitglieder() {
		String[][] tabledata = new String[gruppenMitglieder.size()][SPALTENNAMEN.length];
		for (int i = 0; i < gruppenMitglieder.size(); i++) {
			tabledata[i][0] = gruppenMitglieder.get(i).getVorname();
			tabledata[i][1] = gruppenMitglieder.get(i).getName();
		}
		return tabledata;
	}
}