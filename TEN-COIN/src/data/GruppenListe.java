package data;

import java.util.LinkedList;

public class GruppenListe {

	private LinkedList<Gruppe> gruppenListe;

	public GruppenListe() {
		this.gruppenListe = new LinkedList<Gruppe>();
	}

	public void addGruppenListe(Gruppe g) {
		this.gruppenListe.add(g);
	}

	public LinkedList<Gruppe> getGruppenliste() {
		return gruppenListe;
	}

	public void setGruppenListe(LinkedList<Gruppe> gruppenListe) {
		this.gruppenListe = gruppenListe;
	}

	public String[] getGruppenListe() {
		String[] tabledata = new String[gruppenListe.size()];
		for (int i = 0; i < gruppenListe.size(); i++) {
			tabledata[i] = gruppenListe.get(i).getGruppe();
		}
		return tabledata;
	}
}