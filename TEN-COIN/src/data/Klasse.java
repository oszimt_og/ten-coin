package data;

public class Klasse {
	private String klasse;

	public Klasse() {
		super();
	}

	public Klasse(String klasse) {
		super();
		this.klasse = klasse;
	}

	public String getKlasse() {
		return klasse;
	}

	public void setKlasse(String klasse) {
		this.klasse = klasse;
	}

	@Override
	public String toString() {
		return "Klasse [klasse=" + klasse + "]";
	}
}
