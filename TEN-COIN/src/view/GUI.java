package view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import data.Benutzer;
import data.Gruppe;
import data.GruppenMitglieder;
import data.Klasse;
import data.MusikPlayer;
import data.Passwort;
import data.SchuelerListe;
import data.UeberweisungsListe;
import data.Verwendungszweck;
import data.VerwendungszweckListe;
import logic.Controller;
import java.awt.Label;
import java.awt.SystemColor;

public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private Controller cc;
	private JTextField tfdVorname;
	private JTextField tfdName;
	private String user;
	private String newuser;
	private int entfernen_umbenennen;
	private JTextField tfdEmail1;
	private JTextField tfdEmail2;
	private JTextField tfdEmail3;
	private JTextField tfdCode3;
	private JPanel pnlCenter;
	private JTable tblSchuelerListe;
	private JTable tblGruppenMitglieder;
	private JTable tblVerwendungszwecke;
	private JTable tblMeineGruppenMitglieder;
	private JTable tblUeberweisungsListe;
	private JPasswordField txtPasswort1;
	private JPasswordField txtPasswort2;
	private JPasswordField txtPasswort3;
	private CardLayout cl;
	private JButton btnAbmelden_1;
	private JLabel lblVMeldung;
	private JLabel lblFehler1;
	private JLabel lblFehler2;
	private JLabel lblKlasse;
	private JLabel lblCode;
	private JList klassenAuswahl1;
	private JList klassenAuswahl2;
	private JList lstMeineGruppen;
	private JList lstGruppenListe;
	private JPanel pnlCardRegistrieren;
	private JPanel pnlKlassenArchive;
	private JScrollPane kAuswahl1;
	private JScrollPane kAuswahl2;
	private JScrollPane kMeineGruppen;
	private JScrollPane kGruppenListe;
	private JLabel lblKNP;
	private JLabel lblZahl1;
	private JLabel lblZahl2;
	private JLabel lblZahl3;
	private JLabel lblaZahl1;
	private JLabel lblaZahl2;
	private JLabel lblaZahl3;
	private JLabel lblKontostand2;
	private JLabel lblkeinGuthaben;
	private JTextField tfdVerwendungszweck;
	private JTextField tfdBetrag;
	private JLabel lblEmpfaengerListe;
	private JSlider zWahl1;
	private JSlider zWahl2;
	private JSlider zWahl3;
	private JLabel lblUFehler;
	private JTextField txtNeueGruppe;
	private JTextField txtBestaetigungscode;
	private JTextField txtVerdienst1;
	private JTextField txtVerdienst2;
	private JTextField txtVerdienst3;
	private JTextField txtVerdienst4;
	private JTextField txtVerdienst5;
	private JTextField txtVerdienst6;
	private JTextField txtCode4;
	private JTextField txtCode5;
	private JTextField txtCode6;
	private JLabel lblGruppenMeldungen;
	private JLabel lblGruppenkasse;
	private JTextField txtCode1;
	private JTextField txtCode2;
	private JTextField txtCode3;
	private JProgressBar pbNotenleiste;
	private JLabel lblS;
	private int[] spaltenlaengeUL = {320,60,120};
	private JLabel lblFragLehrer;
	private JTextField txtFragLehrerCode;
	private MusikPlayer m = new MusikPlayer();
	private JLabel lblNewPasswortMeldungen;
	private JButton btnUebernehmen;
	private JButton btnCodeSenden;
	private JTextField txtVBestaetigung;
	private JList klassenAuswahlV;
	private JScrollPane kAuswahlV;
	private JList tblSchuelerListeV;
	private JList lstGruppenListeV;
	private JScrollPane kGruppenListeV;
	private JList lstGruppenMitgliederV;
	private JList lstVerwendungszweckeV;
	private JTextField txtVName;
	private JCheckBox CheckBox_1;
	private JCheckBox CheckBox_2;
	private JCheckBox CheckBox_3;
	private JCheckBox CheckBox_4;
	private JCheckBox CheckBox_5;
	private JCheckBox CheckBox_6;
	private JLabel lblH_HT_L;
	private JLabel lblH_HT_R;
	private JLabel lblPromocode;
	private JTextField txtPromocodeEingabe;
	private JButton btnEinloesen;
	private JList tblSchuelerListeG;
	private int nz;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GUI() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(GUI.class.getResource("/bilder/Logo.png")));
		// Logikschicht
		this.cc = new Controller();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 450);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240, 230, 140));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JLabel lblTenCoin = new JLabel();
		lblTenCoin.setHorizontalAlignment(SwingConstants.CENTER);
		lblTenCoin.setFont(new Font("Algerian", Font.BOLD | Font.ITALIC, 45));
		lblTenCoin.setText("TEN-COIN");
		lblTenCoin.setForeground(Color.RED);
		contentPane.add(lblTenCoin, BorderLayout.NORTH);		

		pnlCenter = new JPanel();
		pnlCenter.setBackground(new Color(240, 230, 140));
		contentPane.add(pnlCenter, BorderLayout.CENTER);
		pnlCenter.setLayout(new CardLayout(0, 0));
		
				
		JPanel pnlCardAnmelden = new JPanel();
		pnlCardAnmelden.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlCardAnmelden, "anmelden");
		pnlCardAnmelden.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlCardAnmelden.setLayout(null);
		
		JLabel lblEmail = new JLabel("E-MAIL");
		lblEmail.setBounds(12, 48, 400, 75);
		lblEmail.setHorizontalAlignment(SwingConstants.CENTER);
		lblEmail.setFont(new Font("Algerian", Font.BOLD, 22));
		pnlCardAnmelden.add(lblEmail);

		tfdEmail1 = new JTextField();
		tfdEmail1.setFont(new Font("Algerian", Font.BOLD, 22));
		tfdEmail1.setBounds(12, 108, 400, 60);
		tfdEmail1.setHorizontalAlignment(SwingConstants.CENTER);
		tfdEmail1.setColumns(10);
		pnlCardAnmelden.add(tfdEmail1);
		
		JLabel lblPasswort = new JLabel("PASSWORT");
		lblPasswort.setFont(new Font("Algerian", Font.BOLD, 22));
		lblPasswort.setHorizontalAlignment(SwingConstants.CENTER);
		lblPasswort.setBounds(512, 48, 250, 75);
		pnlCardAnmelden.add(lblPasswort);
		
		txtPasswort1 = new JPasswordField();
		txtPasswort1.setHorizontalAlignment(SwingConstants.CENTER);
		txtPasswort1.setBounds(512, 108, 250, 60);
		pnlCardAnmelden.add(txtPasswort1);
		
		JButton btnNeuesKonto = new JButton("NEUES KONTO ERSTELLEN");
		btnNeuesKonto.setBounds(547, 289, 215, 35);
		btnNeuesKonto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				klassenAuswahl1.setListData(cc.getKlassenListe().getKlassenListe());
				tfdVorname.setText("");
				tfdName.setText("");
				tfdEmail2.setText("");
				txtPasswort2.setText("");
				lblFehler2.setText("");
				tfdEmail1.setText("");
				txtPasswort1.setText("");
				lblFehler1.setText("");
				cl = (CardLayout) (pnlCenter.getLayout());
				cl.show(pnlCenter, "registrieren");
			}
		});
		pnlCardAnmelden.add(btnNeuesKonto);

		JButton btnAnmelden = new JButton("ANMELDEN");
		btnAnmelden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cl = (CardLayout) (pnlCenter.getLayout());
				try {
					btnAnmelden_Clicked();
				} catch (NoSuchAlgorithmException | InvalidKeySpecException e1) {
				}
			}
		});
		btnAnmelden.setBounds(12, 289, 215, 35);
		pnlCardAnmelden.add(btnAnmelden);
		
		JButton btnPasswortVergessen = new JButton("PASSWORT VERGESSEN?");
		btnPasswortVergessen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cl = (CardLayout) (pnlCenter.getLayout());
				tfdEmail3.setText("");
				tfdEmail3.setEnabled(true);
				tfdCode3.setText("");
				txtPasswort3.setText("");
				lblNewPasswortMeldungen.setText("");
				btnCodeSenden.setEnabled(true);
				btnUebernehmen.setEnabled(false);
				cl.show(pnlCenter, "passwortvergessen");
			}
		});
		btnPasswortVergessen.setBounds(277, 289, 215, 35);
		pnlCardAnmelden.add(btnPasswortVergessen);
		
		lblFehler1 = new JLabel();
		lblFehler1.setForeground(Color.RED);
		lblFehler1.setFont(new Font("Algerian", Font.BOLD, 16));
		lblFehler1.setHorizontalAlignment(SwingConstants.CENTER);
		lblFehler1.setBounds(12, 206, 750, 35);
		pnlCardAnmelden.add(lblFehler1);
		
		
		pnlCardRegistrieren = new JPanel();
		pnlCardRegistrieren.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlCardRegistrieren, "registrieren");
		pnlCardRegistrieren.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlCardRegistrieren.setLayout(null);

		JLabel lblVorname = new JLabel("VORNAME");
		lblVorname.setBounds(12, 13, 215, 75);
		lblVorname.setHorizontalAlignment(SwingConstants.CENTER);
		lblVorname.setFont(new Font("Algerian", Font.BOLD, 22));
		pnlCardRegistrieren.add(lblVorname);

		tfdVorname = new JTextField();
		tfdVorname.setFont(new Font("Algerian", Font.BOLD, 22));
		tfdVorname.setBounds(12, 69, 215, 60);
		tfdVorname.setHorizontalAlignment(SwingConstants.CENTER);
		tfdVorname.setColumns(10);
		pnlCardRegistrieren.add(tfdVorname);
		
		JLabel lblName = new JLabel("NAME");
		lblName.setBounds(281, 13, 215, 75);
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		lblName.setFont(new Font("Algerian", Font.BOLD, 22));
		pnlCardRegistrieren.add(lblName);

		tfdName = new JTextField();
		tfdName.setFont(new Font("Algerian", Font.BOLD, 22));
		tfdName.setBounds(281, 69, 215, 60);
		tfdName.setHorizontalAlignment(SwingConstants.CENTER);
		tfdName.setColumns(10);
		pnlCardRegistrieren.add(tfdName);
		
		lblEmail = new JLabel("E-MAIL");
		lblEmail.setBounds(12, 142, 484, 75);
		lblEmail.setHorizontalAlignment(SwingConstants.CENTER);
		lblEmail.setFont(new Font("Algerian", Font.BOLD, 22));
		pnlCardRegistrieren.add(lblEmail);

		tfdEmail2 = new JTextField();
		tfdEmail2.setFont(new Font("Algerian", Font.BOLD, 22));
		tfdEmail2.setBounds(12, 201, 484, 60);
		tfdEmail2.setHorizontalAlignment(SwingConstants.CENTER);
		tfdEmail2.setColumns(10);
		pnlCardRegistrieren.add(tfdEmail2);
				
		lblPasswort = new JLabel("PASSWORT");
		lblPasswort.setFont(new Font("Algerian", Font.BOLD, 22));
		lblPasswort.setHorizontalAlignment(SwingConstants.CENTER);
		lblPasswort.setBounds(545, 142, 215, 75);
		pnlCardRegistrieren.add(lblPasswort);
		
		txtPasswort2 = new JPasswordField();
		txtPasswort2.setHorizontalAlignment(SwingConstants.CENTER);
		txtPasswort2.setBounds(545, 203, 215, 60);
		pnlCardRegistrieren.add(txtPasswort2);
		
		lblKlasse = new JLabel("KLASSE");
		lblKlasse.setBounds(545, 13, 215, 75);
		lblKlasse.setHorizontalAlignment(SwingConstants.CENTER);
		lblKlasse.setFont(new Font("Algerian", Font.BOLD, 22));
		pnlCardRegistrieren.add(lblKlasse);
		
		JButton btnRegistrieren = new JButton("REGISTRIEREN");
		btnRegistrieren.setBounds(12, 289, 748, 35);
		btnRegistrieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {	
					btnRegistrieren_Clicked();
				} catch (NoSuchAlgorithmException | InvalidKeySpecException e1) {
				}
			}
		});
		pnlCardRegistrieren.add(btnRegistrieren);
		
		klassenAuswahl1 = new JList(cc.getKlassenListe().getKlassenListe());
		klassenAuswahl1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		kAuswahl1 = new JScrollPane(klassenAuswahl1);
		pnlCardRegistrieren.add(kAuswahl1);
		kAuswahl1.setBounds(545, 69, 215, 60);
		kAuswahl1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		lblFehler2 = new JLabel("Gib bitte deine richtige Daten ein, damit die Benotung erfolgen kann!!!");
		lblFehler2.setForeground(Color.RED);
		lblFehler2.setFont(new Font("Algerian", Font.BOLD, 16));
		lblFehler2.setHorizontalAlignment(SwingConstants.CENTER);
		lblFehler2.setBounds(12, 142, 748, 16);
		pnlCardRegistrieren.add(lblFehler2);
		
		JPanel pnlCardCode = new JPanel();
		pnlCardCode.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlCardCode, "Code");
		pnlCardCode.setLayout(null);
		
		lblFragLehrer = new JLabel();
		lblFragLehrer.setBounds(0, 15, 772, 56);
		lblFragLehrer.setHorizontalAlignment(SwingConstants.CENTER);
		lblFragLehrer.setForeground(Color.RED);
		lblFragLehrer.setFont(new Font("Algerian", Font.BOLD, 22));
		pnlCardCode.add(lblFragLehrer);
		
		JLabel lblFragLehrerCode = new JLabel("CODE");
		lblFragLehrerCode.setHorizontalAlignment(SwingConstants.CENTER);
		lblFragLehrerCode.setFont(new Font("Algerian", Font.BOLD, 26));
		lblFragLehrerCode.setBounds(0, 96, 772, 30);
		pnlCardCode.add(lblFragLehrerCode);
		
		txtFragLehrerCode = new JTextField();
		txtFragLehrerCode.setHorizontalAlignment(SwingConstants.CENTER);
		txtFragLehrerCode.setFont(new Font("Algerian", Font.BOLD, 22));
		txtFragLehrerCode.setBounds(325, 139, 123, 22);
		pnlCardCode.add(txtFragLehrerCode);
		txtFragLehrerCode.setColumns(10);
		
		JButton btnBestaetigenFL = new JButton("Best\u00E4tigen");
		btnBestaetigenFL.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (cc.validateCode(newuser, Integer.parseInt(txtFragLehrerCode.getText()))) {
				newuser = null;
				cl.first(pnlCenter);
				}else {
				m.dateiAnspielen("audio/FalscherCode.mp3");
				}
			}
		});
		btnBestaetigenFL.setFont(new Font("Algerian", Font.BOLD, 16));
		btnBestaetigenFL.setBounds(275, 174, 216, 25);
		pnlCardCode.add(btnBestaetigenFL);
		
		JButton btnAbbrechenFL = new JButton("Abbrechen");
		btnAbbrechenFL.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			cc.deleteBenutzer(newuser);
			cl.first(pnlCenter);
			}
		});
		btnAbbrechenFL.setFont(new Font("Algerian", Font.BOLD, 16));
		btnAbbrechenFL.setBounds(235, 212, 292, 25);
		pnlCardCode.add(btnAbbrechenFL);
		
		JLabel lbl22L = new JLabel();
		lbl22L.setIcon(new ImageIcon("./TEN-COIN/src/bilder/22.png"));
		lbl22L.setBounds(260, 260, 100, 60);
		pnlCardCode.add(lbl22L);
		
		JLabel lbl22R = new JLabel();
		lbl22R.setIcon(new ImageIcon("./TEN-COIN/src/bilder/22.png"));
		lbl22R.setBounds(412, 260, 100, 60);
		pnlCardCode.add(lbl22R);
		
		JLabel lblPL = new JLabel("/");
		lblPL.setForeground(new Color(0, 128, 0));
		lblPL.setFont(new Font("Tahoma", Font.BOLD, 99));
		lblPL.setBounds(203, 220, 67, 100);
		pnlCardCode.add(lblPL);
		
		JLabel lblPR = new JLabel("\\");
		lblPR.setForeground(new Color(0, 128, 0));
		lblPR.setFont(new Font("Tahoma", Font.BOLD, 99));
		lblPR.setBounds(502, 220, 67, 100);
		pnlCardCode.add(lblPR);
		
		JLabel lblPZ = new JLabel("|");
		lblPZ.setForeground(new Color(0, 128, 0));
		lblPZ.setHorizontalAlignment(SwingConstants.CENTER);
		lblPZ.setFont(new Font("Tahoma", Font.BOLD, 99));
		lblPZ.setBounds(0, 220, 772, 100);
		pnlCardCode.add(lblPZ);
		
		JLabel lblSternL = new JLabel();
		lblSternL.setIcon(new ImageIcon("./TEN-COIN/src/bilder/SternL.png"));
		lblSternL.setBounds(130, 96, 225, 125);
		pnlCardCode.add(lblSternL);
		
		JLabel lblSternR = new JLabel();
		lblSternR.setIcon(new ImageIcon("./TEN-COIN/src/bilder/SternR.png"));
		lblSternR.setBounds(500, 96, 225, 125);
		pnlCardCode.add(lblSternR);
		
		JPanel pnlCardPasswortVergessen = new JPanel();
		pnlCardPasswortVergessen.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlCardPasswortVergessen, "passwortvergessen");
		pnlCardPasswortVergessen.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlCardPasswortVergessen.setLayout(null);
		
		lblEmail = new JLabel("E-MAIL");
		lblEmail.setBounds(12, 13, 484, 75);
		lblEmail.setHorizontalAlignment(SwingConstants.CENTER);
		lblEmail.setFont(new Font("Algerian", Font.BOLD, 22));
		pnlCardPasswortVergessen.add(lblEmail);

		tfdEmail3 = new JTextField();
		tfdEmail3.setFont(new Font("Algerian", Font.BOLD, 22));
		tfdEmail3.setBounds(12, 73, 484, 60);
		tfdEmail3.setHorizontalAlignment(SwingConstants.CENTER);
		tfdEmail3.setColumns(10);
		pnlCardPasswortVergessen.add(tfdEmail3);
		
		lblCode = new JLabel("CODE");
		lblCode.setBounds(12, 194, 215, 75);
		lblCode.setHorizontalAlignment(SwingConstants.CENTER);
		lblCode.setFont(new Font("Algerian", Font.BOLD, 22));
		pnlCardPasswortVergessen.add(lblCode);

		tfdCode3 = new JTextField();
		tfdCode3.setFont(new Font("Algerian", Font.BOLD, 22));
		tfdCode3.setBounds(12, 249, 215, 60);
		tfdCode3.setHorizontalAlignment(SwingConstants.CENTER);
		tfdCode3.setColumns(10);
		pnlCardPasswortVergessen.add(tfdCode3);
		
		lblPasswort = new JLabel("PASSWORT");
		lblPasswort.setFont(new Font("Algerian", Font.BOLD, 22));
		lblPasswort.setHorizontalAlignment(SwingConstants.CENTER);
		lblPasswort.setBounds(281, 194, 215, 75);
		pnlCardPasswortVergessen.add(lblPasswort);
		
		txtPasswort3 = new JPasswordField();
		txtPasswort3.setHorizontalAlignment(SwingConstants.CENTER);
		txtPasswort3.setBounds(281, 251, 215, 60);
		pnlCardPasswortVergessen.add(txtPasswort3);
		
		btnCodeSenden = new JButton("CODE SENDEN");
		btnCodeSenden.setBounds(560, 88, 200, 35);
		btnCodeSenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			if (cc.setgetCode(tfdEmail3.getText()).equals("-1")) {lblNewPasswortMeldungen.setText("Bisher ist kein Konto mit diesem E-Mail verkn\u00FCpft worden!!!");}
			else {
			lblNewPasswortMeldungen.setText("GEH BITTE ZU DEINEM LEHRER UND FRAG IHN NACH DEINEM CODE!!!");
			btnCodeSenden.setEnabled(false);
			tfdEmail3.setEnabled(false);
			btnUebernehmen.setEnabled(true);
			}
			}
		});
		pnlCardPasswortVergessen.add(btnCodeSenden);
		
		btnUebernehmen = new JButton("ÜBERNEHMEN");
		btnUebernehmen.setEnabled(false);
		btnUebernehmen.setBounds(560, 264, 200, 35);
		btnUebernehmen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (cc.validateCode(tfdEmail3.getText(), Integer.parseInt(tfdCode3.getText()))) {
				try {
					Passwort p = new Passwort();
					cc.neuesPasswort(tfdEmail3.getText(), p.setPasswort(txtPasswort3.getText()));
					cl.first(pnlCenter);
				} catch (NoSuchAlgorithmException | InvalidKeySpecException e1) {
				}
			}else {lblNewPasswortMeldungen.setText("Etwas stimmt nicht mit deinem Code!!!");}
			}});
		pnlCardPasswortVergessen.add(btnUebernehmen);
		
		lblNewPasswortMeldungen = new JLabel();
		lblNewPasswortMeldungen.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewPasswortMeldungen.setForeground(Color.RED);
		lblNewPasswortMeldungen.setFont(new Font("Algerian", Font.BOLD, 16));
		lblNewPasswortMeldungen.setBounds(12, 170, 748, 16);
		pnlCardPasswortVergessen.add(lblNewPasswortMeldungen);
		
		JPanel pnlSchuelerAnsicht = new JPanel();
		pnlSchuelerAnsicht.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlSchuelerAnsicht, "SchuelerAnsicht");
		
		tblUeberweisungsListe = new JTable(cc.UeberweisungsListe("").getUeberweisungsListe(), UeberweisungsListe.SPALTENNAMEN);
		tblUeberweisungsListe.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblUeberweisungsListe.setFillsViewportHeight(true);
		tblUeberweisungsListe.setBackground(new Color(240, 230, 140));
		JScrollPane spnUeberweisungsListe = new JScrollPane(tblUeberweisungsListe);
		spnUeberweisungsListe.setBounds(0, 85, 500, 119);
		pnlSchuelerAnsicht.add(spnUeberweisungsListe);
		spnUeberweisungsListe.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JButton btnAbmelden = new JButton("ABMELDEN");
		btnAbmelden.setBounds(550, 310, 200, 25);
		btnAbmelden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tfdEmail1.setText("");
				txtPasswort1.setText("");;
				lblFehler1.setText("");
				CardLayout cl = (CardLayout) (pnlCenter.getLayout());
				cl.first(pnlCenter);
			}
		});
		pnlSchuelerAnsicht.setLayout(null);
		pnlSchuelerAnsicht.add(btnAbmelden);
		
		JLabel lblLogoS = new JLabel();
		lblLogoS.setBounds(550, 13, 200, 200);
		lblLogoS.setIcon(new ImageIcon("./TEN-COIN/src/bilder/Logo.png"));
		pnlSchuelerAnsicht.add(lblLogoS);
		
		JButton btnGluecksspiel = new JButton("GL\u00DCCKSSPIEL");
		btnGluecksspiel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblKontostand2.setText(String.valueOf(cc.getKontostand(user)) + " IT-$");
				lblS.setText("");
				lblkeinGuthaben.setText("");
				lblZahl1.setText("???");
				lblZahl2.setText("???");
				lblZahl3.setText("???");
				zWahl1.setValue(2);
				zWahl2.setValue(2);
				zWahl3.setValue(2);
				cl.show(pnlCenter, "Gluecksspiel");
			}
		});
		btnGluecksspiel.setFont(new Font("Algerian", Font.BOLD, 22));
		btnGluecksspiel.setForeground(new Color(0, 128, 0));
		btnGluecksspiel.setBounds(550, 220, 200, 45);
		pnlSchuelerAnsicht.add(btnGluecksspiel);
		
		JLabel lblKNPtext = new JLabel("KONTOSTAND -> NOTE -> PROGNOSE");
		lblKNPtext.setFont(new Font("Algerian", Font.BOLD, 20));
		lblKNPtext.setBounds(0, 13, 360, 35);
		pnlSchuelerAnsicht.add(lblKNPtext);
		
		lblKNP = new JLabel();
		lblKNP.setFont(new Font("Algerian", Font.BOLD, 16));
		lblKNP.setBounds(0, 50, 538, 35);
		pnlSchuelerAnsicht.add(lblKNP);
		
		JButton btnMeineGruppen = new JButton("MEINE GRUPPEN");
		btnMeineGruppen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tblSchuelerListeG.setListData(cc.getSchuelerListe(cc.getKlasse(user)).getSchuelerListeV());
				txtNeueGruppe.setText("NAME");
				txtBestaetigungscode.setText("");
				txtCode1.setText("1");
				txtCode2.setText("2");
				txtCode3.setText("3");
				txtCode4.setText("4");
				txtCode5.setText("5");
				txtCode6.setText("6");
				txtVerdienst1.setText("0");
				txtVerdienst2.setText("0");
				txtVerdienst3.setText("0");
				txtVerdienst4.setText("0");
				txtVerdienst5.setText("0");
				txtVerdienst6.setText("0");
				lblGruppenkasse.setText("Gruppenkasse: 0 IT-$");
				txtBestaetigungscode.setText("");
				lblGruppenMeldungen.setText("");
				tblMeineGruppenMitglieder.setModel(new DefaultTableModel(cc.getGruppenMitglieder("").getGruppenMitglieder(), GruppenMitglieder.SPALTENNAMEN));
				lstMeineGruppen.setListData(cc.getGruppenListeB(user).getGruppenListe());
				cl.show(pnlCenter, "MeineGruppen");
			}
		});
		
		btnMeineGruppen.setBounds(550, 275, 200, 25);
		pnlSchuelerAnsicht.add(btnMeineGruppen);
		
		JLabel lblTheorie = new JLabel("Allgemeiner Teil");
		lblTheorie.setFont(new Font("Algerian", Font.BOLD, 16));
		lblTheorie.setForeground(Color.GRAY);
		lblTheorie.setBounds(10, 240, 161, 16);
		pnlSchuelerAnsicht.add(lblTheorie);
		
		CheckBox_1 = new JCheckBox("40 IT-$ Aufgaben im Unterricht (7)");
		CheckBox_1.setEnabled(false);
		CheckBox_1.setBackground(new Color(240, 230, 140));
		CheckBox_1.setBounds(8, 260, 227, 25);
		pnlSchuelerAnsicht.add(CheckBox_1);
		
		CheckBox_2 = new JCheckBox("65 IT-$ Hausaufgaben (5)");
		CheckBox_2.setEnabled(false);
		CheckBox_2.setBackground(new Color(240, 230, 140));
		CheckBox_2.setBounds(8, 285, 227, 25);
		pnlSchuelerAnsicht.add(CheckBox_2);
		
		CheckBox_3 = new JCheckBox("50 IT-$ Tests (2)");
		CheckBox_3.setEnabled(false);
		CheckBox_3.setBackground(new Color(240, 230, 140));
		CheckBox_3.setBounds(8, 310, 227, 25);
		pnlSchuelerAnsicht.add(CheckBox_3);
		
		CheckBox_4 = new JCheckBox("75 IT-$ Kurzdarstellungen (5(+3))");
		CheckBox_4.setEnabled(false);
		CheckBox_4.setBackground(new Color(240, 230, 140));
		CheckBox_4.setBounds(268, 260, 232, 25);
		pnlSchuelerAnsicht.add(CheckBox_4);
		
		CheckBox_5 = new JCheckBox("50 IT-$ Vortrag inkl. Handout (1)");
		CheckBox_5.setEnabled(false);
		CheckBox_5.setBackground(new Color(240, 230, 140));
		CheckBox_5.setBounds(268, 285, 232, 25);
		pnlSchuelerAnsicht.add(CheckBox_5);
		
		CheckBox_6 = new JCheckBox("280 IT-$ Projekt (1)");
		CheckBox_6.setEnabled(false);
		CheckBox_6.setBackground(new Color(240, 230, 140));
		CheckBox_6.setBounds(268, 310, 232, 25);
		pnlSchuelerAnsicht.add(CheckBox_6);
		
		pbNotenleiste = new JProgressBar();
		pbNotenleiste.setOrientation(SwingConstants.VERTICAL);
		pbNotenleiste.setMaximum(594);
		pbNotenleiste.setStringPainted(true);
		pbNotenleiste.setBounds(498, 85, 15, 119);
		pnlSchuelerAnsicht.add(pbNotenleiste);
		
		txtPromocodeEingabe = new JTextField();
		txtPromocodeEingabe.setHorizontalAlignment(SwingConstants.CENTER);
		txtPromocodeEingabe.setText("(ZUERST DIE LEISTUNG AUSW\u00C4HLEN)");
		txtPromocodeEingabe.setFont(new Font("Snap ITC", Font.PLAIN, 13));
		txtPromocodeEingabe.setBounds(122, 205, 345, 22);
		pnlSchuelerAnsicht.add(txtPromocodeEingabe);
		txtPromocodeEingabe.setColumns(10);
		
		JLabel lblSPromocode = new JLabel("PROMOCODE:");
		lblSPromocode.setFont(new Font("Showcard Gothic", Font.BOLD | Font.ITALIC, 16));
		lblSPromocode.setBounds(0, 205, 120, 22);
		pnlSchuelerAnsicht.add(lblSPromocode);
		
		btnEinloesen = new JButton("X");
		btnEinloesen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			codeEinloesen();
			}
		});
		btnEinloesen.setFont(new Font("Snap ITC", Font.BOLD | Font.ITALIC, 12));
		btnEinloesen.setBounds(468, 205, 45, 22);
		pnlSchuelerAnsicht.add(btnEinloesen);
		
		JPanel pnlMeineGruppen = new JPanel();
		pnlMeineGruppen.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlMeineGruppen, "MeineGruppen");
		pnlMeineGruppen.setLayout(null);
		
		JLabel lblGruppenFoto = new JLabel("");
		lblGruppenFoto.setHorizontalAlignment(SwingConstants.CENTER);
		lblGruppenFoto.setBounds(12, 70, 760, 170);
		lblGruppenFoto.setIcon(new ImageIcon("./TEN-COIN/src/bilder/Gruppe.png"));
		pnlMeineGruppen.add(lblGruppenFoto);
		
		JLabel lblMeineGruppenText = new JLabel("MEINE GRUPPEN");
		lblMeineGruppenText.setFont(new Font("Algerian", Font.BOLD, 28));
		lblMeineGruppenText.setHorizontalAlignment(SwingConstants.CENTER);
		lblMeineGruppenText.setBounds(262, 0, 240, 40);
		pnlMeineGruppen.add(lblMeineGruppenText);
		
		lblGruppenMeldungen = new JLabel("");
		lblGruppenMeldungen.setFont(new Font("Algerian", Font.BOLD, 16));
		lblGruppenMeldungen.setForeground(Color.RED);
		lblGruppenMeldungen.setHorizontalAlignment(SwingConstants.CENTER);
		lblGruppenMeldungen.setBounds(12, 300, 748, 30);
		pnlMeineGruppen.add(lblGruppenMeldungen);
		
		JLabel lblNeueGruppe = new JLabel("NEUE GRUPPE");
		lblNeueGruppe.setFont(new Font("Algerian", Font.BOLD, 20));
		lblNeueGruppe.setHorizontalAlignment(SwingConstants.CENTER);
		lblNeueGruppe.setBounds(12, 45, 210, 16);
		pnlMeineGruppen.add(lblNeueGruppe);
		
		txtNeueGruppe = new JTextField();
		txtNeueGruppe.setText("NAME");
		txtNeueGruppe.setFont(new Font("Algerian", Font.PLAIN, 20));
		txtNeueGruppe.setHorizontalAlignment(SwingConstants.CENTER);
		txtNeueGruppe.setBounds(12, 65, 210, 25);
		pnlMeineGruppen.add(txtNeueGruppe);
		txtNeueGruppe.setColumns(10);
		
		JButton btnNeueGruppe = new JButton("ERSTELLEN");
		btnNeueGruppe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNeueGruppe_Clicked();
				lstMeineGruppen.setListData(cc.getGruppenListeB(user).getGruppenListe());
			}
		});
		btnNeueGruppe.setFont(new Font("Algerian", Font.BOLD, 16));
		btnNeueGruppe.setBounds(12, 95, 210, 25);
		pnlMeineGruppen.add(btnNeueGruppe);
		
		JLabel lblNeuerMitglied = new JLabel("NEUER MITGLIED");
		lblNeuerMitglied.setHorizontalAlignment(SwingConstants.CENTER);
		lblNeuerMitglied.setFont(new Font("Algerian", Font.BOLD, 20));
		lblNeuerMitglied.setBounds(12, 135, 210, 16);
		pnlMeineGruppen.add(lblNeuerMitglied);
		
		tblSchuelerListeG = new JList(cc.getSchuelerListe("").getSchuelerListeV());
		tblSchuelerListeG.setFont(new Font("Algerian", Font.PLAIN, 20));
		tblSchuelerListeG.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblSchuelerListeG.setBackground(Color.WHITE);
		JScrollPane spnSchuelerListeG = new JScrollPane(tblSchuelerListeG);
		spnSchuelerListeG.setBounds(12, 155, 210, 25);
		pnlMeineGruppen.add(spnSchuelerListeG);
		spnSchuelerListeG.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JButton btnNeuerMitglied = new JButton("EINF\u00DCGEN");
		btnNeuerMitglied.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (tblMeineGruppenMitglieder.getRowCount()<6) {
				if(cc.neueVerbindungBG(String.valueOf(tblSchuelerListeG.getSelectedValue()), String.valueOf(lstMeineGruppen.getSelectedValue()))) { 
				lblGruppenMeldungen.setText("MITGLIED ERFOLGREICH EINGEFÜGT!");
				tblMeineGruppenMitglieder.setModel(new DefaultTableModel(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenMitglieder(), GruppenMitglieder.SPALTENNAMEN));
				}else { lblGruppenMeldungen.setText("FEHLSCHLAG! ÜBERPRÜFEN SIE BITTE, OB E-MAIL UND GRUPPE AUSGEWÄHLT SIND?");}
			}else {lblGruppenMeldungen.setText("FEHLSCHLAG! MAX. ANZAHL DER MITGLIEDER IST SCHON ERREICHT!!!");}}
		});
		btnNeuerMitglied.setFont(new Font("Algerian", Font.BOLD, 16));
		btnNeuerMitglied.setBounds(12, 185, 210, 25);
		pnlMeineGruppen.add(btnNeuerMitglied);
		
		lstMeineGruppen = new JList(cc.getGruppenListeB(user).getGruppenListe());
		lstMeineGruppen.setFont(new Font("Algerian", Font.BOLD, 16));
		lstMeineGruppen.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstMeineGruppen.setBackground(Color.WHITE);
		lstMeineGruppen.setForeground(Color.BLACK);
		lstMeineGruppen.setBounds(503, 0, 35, 185);
		kMeineGruppen = new JScrollPane(lstMeineGruppen);
		kMeineGruppen.setBounds(262, 245, 240, 25);
		kMeineGruppen.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnlMeineGruppen.add(kMeineGruppen);
		
		tblMeineGruppenMitglieder = new JTable(cc.getGruppenMitglieder("").getGruppenMitglieder(), GruppenMitglieder.SPALTENNAMEN);
		tblMeineGruppenMitglieder.setFont(new Font("Algerian", Font.BOLD, 16));
		tblMeineGruppenMitglieder.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblMeineGruppenMitglieder.setFillsViewportHeight(true);
		tblMeineGruppenMitglieder.setBackground(new Color(240, 230, 140));
		JScrollPane spnMeineGruppenMitglieder = new JScrollPane(tblMeineGruppenMitglieder);
		spnMeineGruppenMitglieder.setBounds(544, 69, 171, 119);
		pnlMeineGruppen.add(spnMeineGruppenMitglieder);
		spnMeineGruppenMitglieder.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JButton btnGruppeAuswaehlen = new JButton("GRUPPE AUSW\u00C4HLEN");
		btnGruppeAuswaehlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtBestaetigungscode.setText("");
				txtCode1.setText("1");
				txtCode2.setText("2");
				txtCode3.setText("3");
				txtCode4.setText("4");
				txtCode5.setText("5");
				txtCode6.setText("6");
				tblMeineGruppenMitglieder.setModel(new DefaultTableModel(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenMitglieder(), GruppenMitglieder.SPALTENNAMEN));
				lblGruppenkasse.setText("Gruppenkasse: " + cc.getGruppenkasse(String.valueOf(lstMeineGruppen.getSelectedValue())) + " IT-$");
			}
		});
		
		btnGruppeAuswaehlen.setFont(new Font("Algerian", Font.BOLD, 16));
		btnGruppeAuswaehlen.setBounds(263, 275, 242, 25);
		pnlMeineGruppen.add(btnGruppeAuswaehlen);
		
		lblGruppenkasse = new JLabel("GRUPPENKASSE:");
		lblGruppenkasse.setFont(new Font("Algerian", Font.BOLD, 16));
		lblGruppenkasse.setBounds(544, 47, 228, 16);
		pnlMeineGruppen.add(lblGruppenkasse);
		
		JLabel lblBestaetigungscode = new JLabel("BEST\u00C4TIGUNGSCODE");
		lblBestaetigungscode.setHorizontalAlignment(SwingConstants.CENTER);
		lblBestaetigungscode.setFont(new Font("Algerian", Font.BOLD, 20));
		lblBestaetigungscode.setBounds(12, 225, 210, 16);
		pnlMeineGruppen.add(lblBestaetigungscode);
		
		txtBestaetigungscode = new JTextField();
		txtBestaetigungscode.setHorizontalAlignment(SwingConstants.CENTER);
		txtBestaetigungscode.setFont(new Font("Algerian", Font.PLAIN, 20));
		txtBestaetigungscode.setColumns(10);
		txtBestaetigungscode.setBounds(12, 245, 210, 25);
		pnlMeineGruppen.add(txtBestaetigungscode);
		
		JButton btnAnfoerdern = new JButton("ANF\u00D6RDERN");
		btnAnfoerdern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			txtBestaetigungscode.setText(cc.setgetCode(user));
			}
		});
		btnAnfoerdern.setFont(new Font("Algerian", Font.BOLD, 16));
		btnAnfoerdern.setBounds(12, 275, 210, 25);
		pnlMeineGruppen.add(btnAnfoerdern);
		
		txtVerdienst1 = new JTextField();
		txtVerdienst1.setText("0");
		txtVerdienst1.setHorizontalAlignment(SwingConstants.CENTER);
		txtVerdienst1.setFont(new Font("Algerian", Font.BOLD, 14));
		txtVerdienst1.setBounds(716, 92, 44, 16);
		pnlMeineGruppen.add(txtVerdienst1);
		txtVerdienst1.setColumns(10);
		
		txtVerdienst2 = new JTextField();
		txtVerdienst2.setText("0");
		txtVerdienst2.setHorizontalAlignment(SwingConstants.CENTER);
		txtVerdienst2.setFont(new Font("Algerian", Font.BOLD, 14));
		txtVerdienst2.setColumns(10);
		txtVerdienst2.setBounds(716, 108, 44, 16);
		pnlMeineGruppen.add(txtVerdienst2);
		
		txtVerdienst3 = new JTextField();
		txtVerdienst3.setText("0");
		txtVerdienst3.setHorizontalAlignment(SwingConstants.CENTER);
		txtVerdienst3.setFont(new Font("Algerian", Font.BOLD, 14));
		txtVerdienst3.setColumns(10);
		txtVerdienst3.setBounds(716, 124, 44, 16);
		pnlMeineGruppen.add(txtVerdienst3);
		
		txtVerdienst4 = new JTextField();
		txtVerdienst4.setText("0");
		txtVerdienst4.setHorizontalAlignment(SwingConstants.CENTER);
		txtVerdienst4.setFont(new Font("Algerian", Font.BOLD, 14));
		txtVerdienst4.setColumns(10);
		txtVerdienst4.setBounds(716, 140, 44, 16);
		pnlMeineGruppen.add(txtVerdienst4);
		
		txtVerdienst5 = new JTextField();
		txtVerdienst5.setText("0");
		txtVerdienst5.setHorizontalAlignment(SwingConstants.CENTER);
		txtVerdienst5.setFont(new Font("Algerian", Font.BOLD, 14));
		txtVerdienst5.setColumns(10);
		txtVerdienst5.setBounds(716, 156, 44, 16);
		pnlMeineGruppen.add(txtVerdienst5);
		
		txtVerdienst6 = new JTextField();
		txtVerdienst6.setText("0");
		txtVerdienst6.setHorizontalAlignment(SwingConstants.CENTER);
		txtVerdienst6.setFont(new Font("Algerian", Font.BOLD, 14));
		txtVerdienst6.setColumns(10);
		txtVerdienst6.setBounds(716, 172, 44, 16);
		pnlMeineGruppen.add(txtVerdienst6);
		
		JButton btnAufteilen = new JButton("AUFTEILEN");
		btnAufteilen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int n = cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenMitglieder().length;
				int c = 0;
				if (Integer.parseInt(txtVerdienst1.getText()) < 0 || Integer.parseInt(txtVerdienst2.getText()) < 0 || Integer.parseInt(txtVerdienst3.getText()) < 0 || Integer.parseInt(txtVerdienst4.getText()) < 0 || Integer.parseInt(txtVerdienst5.getText()) < 0 || Integer.parseInt(txtVerdienst6.getText()) < 0){lblGruppenMeldungen.setText("Negative Beträge sind nicht erlaubt!");}
				else {
				int summe = Integer.parseInt(txtVerdienst1.getText()) + Integer.parseInt(txtVerdienst2.getText()) + Integer.parseInt(txtVerdienst3.getText()) + Integer.parseInt(txtVerdienst4.getText()) + Integer.parseInt(txtVerdienst5.getText()) + Integer.parseInt(txtVerdienst6.getText());
				if (summe>cc.getGruppenkasse(String.valueOf(lstMeineGruppen.getSelectedValue()))) {lblGruppenMeldungen.setText("FINANZIELLE GRENZE LEIDER ÜBERSCHRITTEN!");}
				else {
				if(n-1>=0 && cc.validateCode(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenmitglieder().get(0).getEmail(), Integer.parseInt(txtCode1.getText()))) {c++;}
				if(n-1>=1 && cc.validateCode(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenmitglieder().get(1).getEmail(), Integer.parseInt(txtCode2.getText()))) {c++;}
				if(n-1>=2 && cc.validateCode(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenmitglieder().get(2).getEmail(), Integer.parseInt(txtCode3.getText()))) {c++;}
				if(n-1>=3 && cc.validateCode(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenmitglieder().get(3).getEmail(), Integer.parseInt(txtCode4.getText()))) {c++;}
				if(n-1>=4 && cc.validateCode(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenmitglieder().get(4).getEmail(), Integer.parseInt(txtCode5.getText()))) {c++;}
				if(n-1>=5 && cc.validateCode(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenmitglieder().get(5).getEmail(), Integer.parseInt(txtCode6.getText()))) {c++;}
				if(c<n) {lblGruppenMeldungen.setText("Alle müssen mit Aufteilung einverstanden sein!");}
				else {
					Verwendungszweck v = new Verwendungszweck("Deine Gruppe belohnte dich für deine Leistungen.");
					cc.neuerVerwendungszweck(v);
					if(n-1>=0 && Integer.parseInt(txtVerdienst1.getText())>0) {cc.istUeberwiesenB(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenmitglieder().get(0).getEmail(), v.getVerwendungszweck(), Integer.parseInt(txtVerdienst1.getText()));}
					if(n-1>=1 && Integer.parseInt(txtVerdienst2.getText())>0) {cc.istUeberwiesenB(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenmitglieder().get(1).getEmail(), v.getVerwendungszweck(), Integer.parseInt(txtVerdienst2.getText()));}
					if(n-1>=2 && Integer.parseInt(txtVerdienst3.getText())>0) {cc.istUeberwiesenB(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenmitglieder().get(2).getEmail(), v.getVerwendungszweck(), Integer.parseInt(txtVerdienst3.getText()));}
					if(n-1>=3 && Integer.parseInt(txtVerdienst4.getText())>0) {cc.istUeberwiesenB(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenmitglieder().get(3).getEmail(), v.getVerwendungszweck(), Integer.parseInt(txtVerdienst4.getText()));}
					if(n-1>=4 && Integer.parseInt(txtVerdienst5.getText())>0) {cc.istUeberwiesenB(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenmitglieder().get(4).getEmail(), v.getVerwendungszweck(), Integer.parseInt(txtVerdienst5.getText()));}
					if(n-1>=5 && Integer.parseInt(txtVerdienst6.getText())>0) {cc.istUeberwiesenB(cc.getGruppenMitglieder(String.valueOf(lstMeineGruppen.getSelectedValue())).getGruppenmitglieder().get(5).getEmail(), v.getVerwendungszweck(), Integer.parseInt(txtVerdienst6.getText()));}
					cc.istUeberwiesenG(String.valueOf(lstMeineGruppen.getSelectedValue()), (-1)*summe);
					lblGruppenkasse.setText("Gruppenkasse: " + String.valueOf(cc.getGruppenkasse(String.valueOf(lstMeineGruppen.getSelectedValue()))) + " IT-$");
					lblGruppenMeldungen.setText("");
					txtBestaetigungscode.setText("");
					txtCode1.setText("1");
					txtCode2.setText("2");
					txtCode3.setText("3");
					txtCode4.setText("4");
					txtCode5.setText("5");
					txtCode6.setText("6");
					txtVerdienst1.setText("0");
					txtVerdienst2.setText("0");
					txtVerdienst3.setText("0");
					txtVerdienst4.setText("0");
					txtVerdienst5.setText("0");
					txtVerdienst6.setText("0");
				}
				}
				}
			}
		});
		btnAufteilen.setFont(new Font("Algerian", Font.BOLD, 16));
		btnAufteilen.setBounds(544, 275, 216, 25);
		pnlMeineGruppen.add(btnAufteilen);
		
		txtCode1 = new JTextField();
		txtCode1.setText("1");
		txtCode1.setHorizontalAlignment(SwingConstants.CENTER);
		txtCode1.setFont(new Font("Algerian", Font.BOLD, 16));
		txtCode1.setColumns(10);
		txtCode1.setBounds(544, 215, 69, 25);
		pnlMeineGruppen.add(txtCode1);
		
		txtCode2 = new JTextField();
		txtCode2.setText("2");
		txtCode2.setHorizontalAlignment(SwingConstants.CENTER);
		txtCode2.setFont(new Font("Algerian", Font.BOLD, 16));
		txtCode2.setColumns(10);
		txtCode2.setBounds(618, 215, 69, 25);
		pnlMeineGruppen.add(txtCode2);
		
		txtCode3 = new JTextField();
		txtCode3.setText("3");
		txtCode3.setHorizontalAlignment(SwingConstants.CENTER);
		txtCode3.setFont(new Font("Algerian", Font.BOLD, 16));
		txtCode3.setColumns(10);
		txtCode3.setBounds(691, 215, 69, 25);
		pnlMeineGruppen.add(txtCode3);
		
		txtCode4 = new JTextField();
		txtCode4.setText("4");
		txtCode4.setHorizontalAlignment(SwingConstants.CENTER);
		txtCode4.setFont(new Font("Algerian", Font.BOLD, 16));
		txtCode4.setColumns(10);
		txtCode4.setBounds(544, 243, 69, 25);
		pnlMeineGruppen.add(txtCode4);
		
		txtCode5 = new JTextField();
		txtCode5.setText("5");
		txtCode5.setHorizontalAlignment(SwingConstants.CENTER);
		txtCode5.setFont(new Font("Algerian", Font.BOLD, 16));
		txtCode5.setColumns(10);
		txtCode5.setBounds(618, 243, 69, 25);
		pnlMeineGruppen.add(txtCode5);
		
		txtCode6 = new JTextField();
		txtCode6.setText("6");
		txtCode6.setHorizontalAlignment(SwingConstants.CENTER);
		txtCode6.setFont(new Font("Algerian", Font.BOLD, 16));
		txtCode6.setColumns(10);
		txtCode6.setBounds(691, 243, 69, 25);
		pnlMeineGruppen.add(txtCode6);
		
		JLabel lblVerdienstWaehrung = new JLabel("IT-$");
		lblVerdienstWaehrung.setHorizontalAlignment(SwingConstants.CENTER);
		lblVerdienstWaehrung.setFont(new Font("Algerian", Font.BOLD, 18));
		lblVerdienstWaehrung.setBounds(716, 70, 44, 21);
		pnlMeineGruppen.add(lblVerdienstWaehrung);
		
		JLabel lblBestaetigungcodes = new JLabel("BEST\u00C4TIGUNGSCODES");
		lblBestaetigungcodes.setFont(new Font("Algerian", Font.BOLD, 18));
		lblBestaetigungcodes.setHorizontalAlignment(SwingConstants.CENTER);
		lblBestaetigungcodes.setBounds(544, 194, 216, 16);
		pnlMeineGruppen.add(lblBestaetigungcodes);
		
		JButton btnBack1 = new JButton("<");
		btnBack1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				zurueckSchuelerAnsicht();
			}
		});
		btnBack1.setFont(new Font("Algerian", Font.BOLD, 16));
		btnBack1.setBounds(222, 7, 43, 25);
		pnlMeineGruppen.add(btnBack1);
		
		JButton btnBack2 = new JButton("<");
		btnBack2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				zurueckSchuelerAnsicht();
			}
		});
		btnBack2.setFont(new Font("Algerian", Font.BOLD, 16));
		btnBack2.setBounds(500, 7, 43, 25);
		pnlMeineGruppen.add(btnBack2);
		
		JPanel pnlGluecksspiel = new JPanel();
		pnlGluecksspiel.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlGluecksspiel, "Gluecksspiel");
		pnlGluecksspiel.setLayout(null);
		
		lblZahl1 = new JLabel("???");
		lblZahl1.setFont(new Font("Algerian", Font.BOLD, 20));
		lblZahl1.setHorizontalAlignment(SwingConstants.CENTER);
		lblZahl1.setBounds(200, 115, 100, 20);
		pnlGluecksspiel.add(lblZahl1);
		
		lblZahl2 = new JLabel("???");
		lblZahl2.setFont(new Font("Algerian", Font.BOLD, 20));
		lblZahl2.setHorizontalAlignment(SwingConstants.CENTER);
		lblZahl2.setBounds(350, 115, 100, 20);
		pnlGluecksspiel.add(lblZahl2);
		
		lblZahl3 = new JLabel("???");
		lblZahl3.setHorizontalAlignment(SwingConstants.CENTER);
		lblZahl3.setFont(new Font("Algerian", Font.BOLD, 20));
		lblZahl3.setBounds(500, 115, 100, 20);
		pnlGluecksspiel.add(lblZahl3);
		
		zWahl1 = new JSlider(JSlider.HORIZONTAL, 1, 3, 2);
		lblaZahl1 = new JLabel(String.valueOf(zWahl1.getValue()));
		zWahl1.setBounds(200, 150, 100, 30);
		lblaZahl1.setFont(new Font("Algerian", Font.BOLD, 20));
		lblaZahl1.setHorizontalAlignment(SwingConstants.CENTER);
		lblaZahl1.setBounds(200, 185, 100, 20);
		zWahl1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				lblaZahl1.setText(String.valueOf(zWahl1.getValue()));
			}
		});
		pnlGluecksspiel.add(zWahl1);
		pnlGluecksspiel.add(lblaZahl1);
		
		zWahl2 = new JSlider(JSlider.HORIZONTAL, 1, 3, 2);
		zWahl2.setBounds(350, 150, 100, 30);
		lblaZahl2 = new JLabel(String.valueOf(zWahl2.getValue()));
		lblaZahl2.setFont(new Font("Algerian", Font.BOLD, 20));
		lblaZahl2.setHorizontalAlignment(SwingConstants.CENTER);
		lblaZahl2.setBounds(350, 185, 100, 20);
		zWahl2.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				lblaZahl2.setText(String.valueOf(zWahl2.getValue()));
			}
		});
		pnlGluecksspiel.add(zWahl2);
		pnlGluecksspiel.add(lblaZahl2);
		
		zWahl3 = new JSlider(JSlider.HORIZONTAL, 1, 3, 2);
		zWahl3.setBounds(500, 150, 100, 30);
		lblaZahl3 = new JLabel(String.valueOf(zWahl3.getValue()));
		lblaZahl3.setFont(new Font("Algerian", Font.BOLD, 20));
		lblaZahl3.setHorizontalAlignment(SwingConstants.CENTER);
		lblaZahl3.setBounds(500, 185, 100, 20);
		zWahl3.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				lblaZahl3.setText(String.valueOf(zWahl3.getValue()));
			}
		});
		pnlGluecksspiel.add(zWahl3);
		pnlGluecksspiel.add(lblaZahl3);
		
		JLabel lblRegeln = new JLabel("Regeln -> Spieler w\u00E4hlt drei Zahlen(1-3) -> 1 Versuch kostet, wie die h\u00F6chste ausgew\u00E4hlte Zahl -> Spieler erh\u00E4lt die Summe der getroffenen Zahlen");
		lblRegeln.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegeln.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblRegeln.setBounds(0, 300, 772, 30);
		pnlGluecksspiel.add(lblRegeln);
		
		JButton btnPlay = new JButton("PLAY");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnPlay_Clicked();
			}
		});
		btnPlay.setFont(new Font("Algerian", Font.BOLD, 22));
		btnPlay.setBounds(200, 235, 400, 30);
		pnlGluecksspiel.add(btnPlay);
		
		JLabel lblGruenerWeg = new JLabel("\"GR\u00DCNER WEG\"");
		lblGruenerWeg.setForeground(new Color(0, 128, 0));
		lblGruenerWeg.setFont(new Font("Algerian", Font.BOLD, 40));
		lblGruenerWeg.setHorizontalAlignment(SwingConstants.CENTER);
		lblGruenerWeg.setBounds(12, 13, 748, 30);
		pnlGluecksspiel.add(lblGruenerWeg);
		
		JLabel lblStatus = new JLabel("KONTOSTAND:");
		lblStatus.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
		lblStatus.setBounds(12, 66, 100, 16);
		pnlGluecksspiel.add(lblStatus);
		
		lblKontostand2 = new JLabel("");
		lblKontostand2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
		lblKontostand2.setBounds(12, 89, 160, 16);
		pnlGluecksspiel.add(lblKontostand2);
		
		lblkeinGuthaben = new JLabel("");
		lblkeinGuthaben.setFont(new Font("Algerian", Font.BOLD, 16));
		lblkeinGuthaben.setHorizontalAlignment(SwingConstants.CENTER);
		lblkeinGuthaben.setForeground(Color.RED);
		lblkeinGuthaben.setBounds(12, 218, 748, 16);
		pnlGluecksspiel.add(lblkeinGuthaben);
		
		JButton btnZurueckG = new JButton("ZUR\u00DCCK");
		btnZurueckG.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				zurueckSchuelerAnsicht();
			}
		});
		btnZurueckG.setBounds(350, 267, 100, 25);
		pnlGluecksspiel.add(btnZurueckG);
		
		lblS = new JLabel("");
		lblS.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
		lblS.setBounds(12, 105, 160, 16);
		pnlGluecksspiel.add(lblS);
		
		JPanel pnlVerwaltung = new JPanel();
		pnlVerwaltung.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlVerwaltung, "Verwaltung");
		pnlVerwaltung.setLayout(null);
		
		JLabel lblVKlasse = new JLabel("Klasse: ");
		lblVKlasse.setFont(new Font("Algerian", Font.BOLD, 20));
		lblVKlasse.setBounds(12, 25, 92, 16);
		pnlVerwaltung.add(lblVKlasse);
		
		klassenAuswahlV = new JList(cc.getKlassenListe().getKlassenListe());
		klassenAuswahlV.setFont(new Font("Tahoma", Font.BOLD, 13));
		klassenAuswahlV.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		klassenAuswahlV.setBackground(new Color(240, 230, 140));
		klassenAuswahlV.setForeground(Color.BLACK);
		kAuswahlV = new JScrollPane(klassenAuswahlV);
		pnlVerwaltung.add(kAuswahlV);
		kAuswahlV.setBounds(102, 22, 70, 22);
		kAuswahlV.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JButton btnNewButton = new JButton("AUSW\u00C4HLEN");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			tblSchuelerListeV.setListData(cc.getSchuelerListe(String.valueOf(klassenAuswahlV.getSelectedValue())).getSchuelerListeV());
			lstGruppenListeV.setListData(cc.getGruppenListeK(String.valueOf(klassenAuswahlV.getSelectedValue())).getGruppenListe());
			}
		});
		btnNewButton.setFont(new Font("Algerian", Font.PLAIN, 12));
		btnNewButton.setBounds(210, 10, 150, 15);
		pnlVerwaltung.add(btnNewButton);
		
		JButton btnLschen = new JButton("Entfernen");
		btnLschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblVMeldung.setText("!!!WARNUNG!!!Nicht löschen, wenn es relevant ist!Bestätigungscode eingeben!");
			entfernen_umbenennen = 1;
			}
		});
		btnLschen.setFont(new Font("Algerian", Font.PLAIN, 12));
		btnLschen.setBounds(210, 40, 150, 15);
		pnlVerwaltung.add(btnLschen);
		
		JButton btnUmbennen = new JButton("Umbenennen");
		btnUmbennen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblVMeldung.setText("Geben Sie bitte den neuen Namen, sowie den Bestätigungscode ein!!!");
			entfernen_umbenennen = 6;
			}
		});
		btnUmbennen.setFont(new Font("Algerian", Font.PLAIN, 12));
		btnUmbennen.setBounds(288, 25, 108, 15);
		pnlVerwaltung.add(btnUmbennen);
		
		JLabel lblVSchueler = new JLabel("Schuler: ");
		lblVSchueler.setFont(new Font("Algerian", Font.BOLD, 20));
		lblVSchueler.setBounds(12, 70, 103, 16);
		pnlVerwaltung.add(lblVSchueler);
		
		tblSchuelerListeV = new JList(cc.getSchuelerListe("").getSchuelerListeV());
		tblSchuelerListeV.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblSchuelerListeV.setBackground(new Color(240, 230, 140));
		JScrollPane spnSchuelerListeV = new JScrollPane(tblSchuelerListeV);
		spnSchuelerListeV.setBounds(116, 67, 502, 22);
		pnlVerwaltung.add(spnSchuelerListeV);
		spnSchuelerListeV.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JButton btnEntfernen = new JButton("Entfernen");
		btnEntfernen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblVMeldung.setText("!!!WARNUNG!!!Nicht löschen, wenn es relevant ist!Bestätigungscode eingeben!");
			entfernen_umbenennen = 2;
			}
		});
		btnEntfernen.setFont(new Font("Algerian", Font.PLAIN, 12));
		btnEntfernen.setBounds(630, 80, 120, 15);
		pnlVerwaltung.add(btnEntfernen);
		
		JLabel lblVGruppe = new JLabel("Gruppe: ");
		lblVGruppe.setFont(new Font("Algerian", Font.BOLD, 20));
		lblVGruppe.setBounds(12, 120, 95, 16);
		pnlVerwaltung.add(lblVGruppe);
		
		lstGruppenListeV = new JList(cc.getGruppenListeK(String.valueOf(klassenAuswahlV.getSelectedValue())).getGruppenListe());
		lstGruppenListeV.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lstGruppenListeV.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstGruppenListeV.setBackground(new Color(240, 230, 140));
		lstGruppenListeV.setForeground(Color.BLACK);
		lstGruppenListeV.setBounds(238, 125, 109, 103);
		kGruppenListeV = new JScrollPane(lstGruppenListeV);
		kGruppenListeV.setBounds(107, 117, 511, 22);
		kGruppenListeV.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnlVerwaltung.add(kGruppenListeV);
		
		JButton button_2 = new JButton("AUSW\u00C4HLEN");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lstGruppenMitgliederV.setListData(cc.getGruppenMitglieder(String.valueOf(lstGruppenListeV.getSelectedValue())).getGruppenMitgliederV());
			}
		});
		button_2.setFont(new Font("Algerian", Font.PLAIN, 12));
		button_2.setBounds(630, 105, 120, 15);
		pnlVerwaltung.add(button_2);
		
		JButton btnEntfernen_2 = new JButton("Entfernen");
		btnEntfernen_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblVMeldung.setText("!!!WARNUNG!!!Nicht löschen, wenn es relevant ist!Bestätigungscode eingeben!");
			entfernen_umbenennen = 3;
			}
		});
		btnEntfernen_2.setFont(new Font("Algerian", Font.PLAIN, 12));
		btnEntfernen_2.setBounds(630, 135, 120, 15);
		pnlVerwaltung.add(btnEntfernen_2);
		
		JLabel lblVGruppenmitglied = new JLabel("Gruppenmitglied: ");
		lblVGruppenmitglied.setFont(new Font("Algerian", Font.BOLD, 20));
		lblVGruppenmitglied.setBounds(12, 165, 202, 16);
		pnlVerwaltung.add(lblVGruppenmitglied);
		
		lstGruppenMitgliederV = new JList(cc.getGruppenMitglieder("").getGruppenMitgliederV());
		lstGruppenMitgliederV.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lstGruppenMitgliederV.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstGruppenMitgliederV.setBackground(new Color(240, 230, 140));
		JScrollPane spnGruppenMitgliederV = new JScrollPane(lstGruppenMitgliederV);
		spnGruppenMitgliederV.setBounds(211, 162, 407, 22);
		pnlVerwaltung.add(spnGruppenMitgliederV);
		spnGruppenMitgliederV.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JButton btnEntfernen_1 = new JButton("Entfernen");
		btnEntfernen_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblVMeldung.setText("!!!WARNUNG!!!Nicht löschen, wenn es relevant ist!Bestätigungscode eingeben!");
			entfernen_umbenennen = 4;
			}
		});
		btnEntfernen_1.setFont(new Font("Algerian", Font.PLAIN, 12));
		btnEntfernen_1.setBounds(630, 175, 120, 15);
		pnlVerwaltung.add(btnEntfernen_1);
		
		JLabel lblVVerwendungszweck = new JLabel("Verwendungszweck: ");
		lblVVerwendungszweck.setFont(new Font("Algerian", Font.BOLD, 20));
		lblVVerwendungszweck.setBounds(12, 210, 233, 16);
		pnlVerwaltung.add(lblVVerwendungszweck);
		
		lstVerwendungszweckeV = new JList(cc.getVerwendungszweckListe().getVerwendungszweckListeV());
		lstVerwendungszweckeV.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstVerwendungszweckeV.setBackground(new Color(240, 230, 140));
		JScrollPane spnVerwendungszweckeV = new JScrollPane(lstVerwendungszweckeV);
		spnVerwendungszweckeV.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		spnVerwendungszweckeV.setBounds(247, 207, 371, 22);
		pnlVerwaltung.add(spnVerwendungszweckeV);
		
		JButton btnUmbenennen = new JButton("UMBENENNEN");
		btnUmbenennen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblVMeldung.setText("Geben Sie bitte den neuen Namen, sowie den Bestätigungscode ein!!!");
			entfernen_umbenennen = 8;
			}
		});
		btnUmbenennen.setFont(new Font("Algerian", Font.PLAIN, 12));
		btnUmbenennen.setBounds(630, 205, 120, 15);
		pnlVerwaltung.add(btnUmbenennen);
		
		JButton button_5 = new JButton("Entfernen");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblVMeldung.setText("!!!WARNUNG!!!Nicht löschen, wenn es relevant ist!Bestätigungscode eingeben!");
			entfernen_umbenennen = 5;
			}
		});
		button_5.setFont(new Font("Algerian", Font.PLAIN, 12));
		button_5.setBounds(630, 220, 120, 15);
		pnlVerwaltung.add(button_5);
		
		JLabel lblVAllgemeinerTeil = new JLabel("Allgemeiner Teil: ");
		lblVAllgemeinerTeil.setFont(new Font("Algerian", Font.BOLD, 20));
		lblVAllgemeinerTeil.setBounds(12, 255, 210, 16);
		pnlVerwaltung.add(lblVAllgemeinerTeil);
		
		String [] AT = {"40 IT-$ Aufgaben im Unterricht (7)", "65 IT-$ Hausaufgaben (5)", "50 IT-$ Tests (2)", "75 IT-$ Kurzdarstellungen (5(+3))", "50 IT-$ Vortrag inkl. Handout (1)", "280 IT-$ Projekt (1)"}; 
		JList lstAT = new JList(AT);
		lstAT.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lstAT.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstAT.setBackground(new Color(240, 230, 140));
		JScrollPane spnAT = new JScrollPane(lstAT);
		spnAT.setBounds(218, 252, 400, 22);
		pnlVerwaltung.add(spnAT);
		spnAT.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JButton btnErledigt = new JButton("Erledigt");
		btnErledigt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cc.setIstErledigt(lstAT.getSelectedIndex(), 1);
				lstAT.clearSelection();
			}
		});
		btnErledigt.setFont(new Font("Algerian", Font.PLAIN, 12));
		btnErledigt.setBounds(630, 250, 120, 15);
		pnlVerwaltung.add(btnErledigt);
		
		JButton btnNichtErledigt = new JButton("Unerledigt");
		btnNichtErledigt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cc.setIstErledigt(lstAT.getSelectedIndex(), 0);
				lstAT.clearSelection();
			}
		});
		btnNichtErledigt.setFont(new Font("Algerian", Font.PLAIN, 12));
		btnNichtErledigt.setBounds(630, 265, 120, 15);
		pnlVerwaltung.add(btnNichtErledigt);
		
		JLabel lblStriche1 = new JLabel("------------------------------------------------------------------------------------------------");
		lblStriche1.setHorizontalAlignment(SwingConstants.CENTER);
		lblStriche1.setFont(new Font("Algerian", Font.BOLD, 22));
		lblStriche1.setBounds(0, 0, 772, 15);
		pnlVerwaltung.add(lblStriche1);
		
		JLabel lblStriche2 = new JLabel("------------------------------------------------------------------------------------------------");
		lblStriche2.setHorizontalAlignment(SwingConstants.CENTER);
		lblStriche2.setFont(new Font("Algerian", Font.BOLD, 22));
		lblStriche2.setBounds(0, 50, 772, 15);
		pnlVerwaltung.add(lblStriche2);
		
		JLabel lblStriche3 = new JLabel("------------------------------------------------------------------------------------------------");
		lblStriche3.setHorizontalAlignment(SwingConstants.CENTER);
		lblStriche3.setFont(new Font("Algerian", Font.BOLD, 22));
		lblStriche3.setBounds(0, 95, 772, 15);
		pnlVerwaltung.add(lblStriche3);
		
		JLabel lblStriche4 = new JLabel("------------------------------------------------------------------------------------------------");
		lblStriche4.setHorizontalAlignment(SwingConstants.CENTER);
		lblStriche4.setFont(new Font("Algerian", Font.BOLD, 22));
		lblStriche4.setBounds(0, 145, 772, 15);
		pnlVerwaltung.add(lblStriche4);
		
		JLabel lblStriche5 = new JLabel("------------------------------------------------------------------------------------------------");
		lblStriche5.setHorizontalAlignment(SwingConstants.CENTER);
		lblStriche5.setFont(new Font("Algerian", Font.BOLD, 22));
		lblStriche5.setBounds(0, 190, 772, 15);
		pnlVerwaltung.add(lblStriche5);
		
		JLabel lblStriche6 = new JLabel("------------------------------------------------------------------------------------------------");
		lblStriche6.setHorizontalAlignment(SwingConstants.CENTER);
		lblStriche6.setFont(new Font("Algerian", Font.BOLD, 22));
		lblStriche6.setBounds(0, 235, 772, 15);
		pnlVerwaltung.add(lblStriche6);
		
		JLabel lblStriche7 = new JLabel("------------------------------------------------------------------------------------------------");
		lblStriche7.setHorizontalAlignment(SwingConstants.CENTER);
		lblStriche7.setFont(new Font("Algerian", Font.BOLD, 22));
		lblStriche7.setBounds(0, 280, 772, 15);
		pnlVerwaltung.add(lblStriche7);
		
		JLabel lblStriche8 = new JLabel("------------------------------------------------------------------------------------------------");
		lblStriche8.setHorizontalAlignment(SwingConstants.CENTER);
		lblStriche8.setFont(new Font("Algerian", Font.BOLD, 22));
		lblStriche8.setBounds(0, 300, 772, 15);
		pnlVerwaltung.add(lblStriche8);
		
		JLabel lblStriche9 = new JLabel("|||");
		lblStriche9.setFont(new Font("Chiller", Font.BOLD, 28));
		lblStriche9.setBounds(255, 310, 36, 23);
		pnlVerwaltung.add(lblStriche9);
		
		JLabel lblVCode = new JLabel("Code: ");
		lblVCode.setFont(new Font("Chiller", Font.BOLD, 28));
		lblVCode.setBounds(465, 21, 153, 23);
		pnlVerwaltung.add(lblVCode);
		
		JLabel lblVBestaetigung = new JLabel("Best\u00E4tigungscode(\"geheim\"):");
		lblVBestaetigung.setFont(new Font("Chiller", Font.BOLD, 28));
		lblVBestaetigung.setBounds(315, 310, 310, 22);
		pnlVerwaltung.add(lblVBestaetigung);
		
		txtVBestaetigung = new JTextField();
		txtVBestaetigung.setBackground(new Color(240, 230, 140));
		txtVBestaetigung.setHorizontalAlignment(SwingConstants.CENTER);
		txtVBestaetigung.setFont(new Font("Chiller", Font.BOLD, 25));
		txtVBestaetigung.setBounds(617, 310, 76, 22);
		pnlVerwaltung.add(txtVBestaetigung);
		txtVBestaetigung.setColumns(10);
		
		JButton btnVBestaetigung = new JButton("X");
		btnVBestaetigung.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (txtVBestaetigung.getText().equals("geheim")) {
					entfernen_umbenennen(entfernen_umbenennen);
					txtVBestaetigung.setText("");
					txtVName.setText("");
				} else {lblVMeldung.setText("Bestätigungscode stimmt nicht!!!");}
			}
		});
		btnVBestaetigung.setFont(new Font("Chiller", Font.BOLD, 22));
		btnVBestaetigung.setBounds(705, 310, 45, 20);
		pnlVerwaltung.add(btnVBestaetigung);
		
		JButton btnVZueruck = new JButton("<-");
		btnVZueruck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			cl.show(pnlCenter, "LehrerAnsicht");	
			}
		});
		btnVZueruck.setFont(new Font("Chiller", Font.BOLD, 30));
		btnVZueruck.setBounds(630, 15, 120, 30);
		pnlVerwaltung.add(btnVZueruck);
		
		JButton btnCode = new JButton("Code");
		btnCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nz=0;
				while (String.valueOf(tblSchuelerListeV.getSelectedValue()).charAt(nz)!='|'){
				nz++;
		}
				lblVCode.setText("Code: " + cc.setgetCode(String.valueOf(tblSchuelerListeV.getSelectedValue()).substring(0, nz)));
			}
		});
		btnCode.setFont(new Font("Algerian", Font.PLAIN, 12));
		btnCode.setBounds(630, 65, 120, 15);
		pnlVerwaltung.add(btnCode);
		
		JButton button_1 = new JButton("Umbenennen");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblVMeldung.setText("Geben Sie bitte den neuen Namen, sowie den Bestätigungscode ein!!!");
			entfernen_umbenennen = 7;
			}
		});
		button_1.setFont(new Font("Algerian", Font.PLAIN, 12));
		button_1.setBounds(630, 120, 120, 15);
		pnlVerwaltung.add(button_1);
		
		JButton button_6 = new JButton("Code");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblVCode.setText("Code: " + cc.setgetCode(String.valueOf(lstGruppenMitgliederV.getSelectedValue())));
			}
		});
		button_6.setFont(new Font("Algerian", Font.PLAIN, 12));
		button_6.setBounds(630, 160, 120, 15);
		pnlVerwaltung.add(button_6);
		
		JLabel label = new JLabel("/*\\");
		label.setFont(new Font("Comic Sans MS", Font.BOLD, 50));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(392, 10, 80, 45);
		pnlVerwaltung.add(label);
		
		JLabel lblName_1 = new JLabel("Name:");
		lblName_1.setFont(new Font("Chiller", Font.BOLD, 28));
		lblName_1.setBounds(12, 310, 61, 22);
		pnlVerwaltung.add(lblName_1);
		
		txtVName = new JTextField();
		txtVName.setHorizontalAlignment(SwingConstants.CENTER);
		txtVName.setFont(new Font("Chiller", Font.BOLD, 25));
		txtVName.setColumns(10);
		txtVName.setBackground(new Color(240, 230, 140));
		txtVName.setBounds(75, 310, 161, 22);
		pnlVerwaltung.add(txtVName);
		
		lblVMeldung = new JLabel();
		lblVMeldung.setHorizontalAlignment(SwingConstants.CENTER);
		lblVMeldung.setForeground(Color.RED);
		lblVMeldung.setFont(new Font("Algerian", Font.BOLD, 16));
		lblVMeldung.setBounds(12, 286, 738, 20);
		pnlVerwaltung.add(lblVMeldung);
		
		JButton btnErstellen = new JButton("Erstellen");
		btnErstellen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				entfernen_umbenennen = 0;
				lblVMeldung.setText("Geben Sie bitte den Namen, sowie den Bestätigungscode ein!!!");
				txtVName.setText("");
			}
		});
		btnErstellen.setFont(new Font("Algerian", Font.PLAIN, 12));
		btnErstellen.setBounds(175, 25, 113, 15);
		pnlVerwaltung.add(btnErstellen);
		
		JPanel pnlLehrerAnsicht = new JPanel();
		pnlLehrerAnsicht.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlLehrerAnsicht, "LehrerAnsicht");
		
		btnAbmelden_1 = new JButton("ABMELDEN");
		btnAbmelden_1.setBounds(550, 310, 200, 25);
		btnAbmelden_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tfdEmail1.setText("");
				txtPasswort1.setText("");;
				lblFehler1.setText("");
				CardLayout cl = (CardLayout) (pnlCenter.getLayout());
				cl.first(pnlCenter);
			}
		});
		pnlLehrerAnsicht.setLayout(null);
		pnlLehrerAnsicht.add(btnAbmelden_1);

		
		JButton btnPromocode = new JButton("PROMOCODE");
		btnPromocode.setBackground(new Color(154, 205, 50));
		btnPromocode.setFont(new Font("Algerian", Font.ITALIC, 18));
		btnPromocode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblPromocode.setText("*************************");
				lblH_HT_L.setIcon(new ImageIcon("./TEN-COIN/src/bilder/H.png"));
				lblH_HT_R.setIcon(new ImageIcon("./TEN-COIN/src/bilder/H.png"));
				CardLayout cl = (CardLayout) (pnlCenter.getLayout());
				cl.show(pnlCenter, "Promocode");
			}
		});
		btnPromocode.setBounds(190, 120, 160, 58);
		pnlLehrerAnsicht.add(btnPromocode);
		
		JButton btnKlassenArchive = new JButton("KLASSENARCHIV");
		btnKlassenArchive.setBackground(new Color(102, 205, 170));
		btnKlassenArchive.setFont(new Font("Algerian", Font.ITALIC, 18));
		btnKlassenArchive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tblSchuelerListe.setModel(new DefaultTableModel(cc.getSchuelerListe("").getSchuelerListe(), SchuelerListe.SPALTENNAMEN));
				klassenAuswahl2.setListData(cc.getKlassenListe().getKlassenListe());
				lstGruppenListe.setListData(cc.getGruppenListeK("").getGruppenListe());
				tblGruppenMitglieder.setModel(new DefaultTableModel(cc.getGruppenMitglieder("").getGruppenMitglieder(), GruppenMitglieder.SPALTENNAMEN));
				lblEmpfaengerListe.setText("");
				tfdBetrag.setText("");
				tfdVerwendungszweck.setText("");
				lblUFehler.setText("");
				cl.show(pnlCenter, "KlassenArchive");
			}
		});
		btnKlassenArchive.setBounds(84, 176, 180, 75);
		pnlLehrerAnsicht.add(btnKlassenArchive);
		
		JButton btnAktuelleKlasse = new JButton("VERWALTUNG");
		btnAktuelleKlasse.setBackground(new Color(102, 205, 170));
		btnAktuelleKlasse.setFont(new Font("Algerian", Font.ITALIC, 18));
		btnAktuelleKlasse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			klassenAuswahlV.setListData(cc.getKlassenListe().getKlassenListe());
			tblSchuelerListeV.setListData(cc.getSchuelerListe("").getSchuelerListeV());
			lstGruppenListeV.setListData(cc.getGruppenListeK(String.valueOf(klassenAuswahlV.getSelectedValue())).getGruppenListe());
			lstGruppenMitgliederV.setListData(cc.getGruppenMitglieder("").getGruppenMitgliederV());
			lstVerwendungszweckeV.setListData(cc.getVerwendungszweckListe().getVerwendungszweckListeV());
			lblVCode.setText("Code: ");
			txtVBestaetigung.setText("");
			txtVName.setText("");
			lblVMeldung.setText("");
			cl.show(pnlCenter, "Verwaltung");
			}
		});
		btnAktuelleKlasse.setBounds(84, 47, 180, 75);
		pnlLehrerAnsicht.add(btnAktuelleKlasse);
		
		
		JLabel lblLogoL = new JLabel();
		lblLogoL.setBounds(550, 50, 200, 200);
		lblLogoL.setIcon(new ImageIcon("./TEN-COIN/src/bilder/Logo.png"));
		pnlLehrerAnsicht.add(lblLogoL);
		
		JLabel lblStriche = new JLabel(" - - - - - - -");
		lblStriche.setForeground(Color.RED);
		lblStriche.setFont(new Font("Algerian", Font.PLAIN, 37));
		lblStriche.setBounds(396, 127, 152, 50);
		pnlLehrerAnsicht.add(lblStriche);
		
		JLabel lblAntriebO = new JLabel("=");
		lblAntriebO.setFont(new Font("Tahoma", Font.PLAIN, 99));
		lblAntriebO.setForeground(Color.ORANGE);
		lblAntriebO.setBounds(27, 49, 114, 70);
		pnlLehrerAnsicht.add(lblAntriebO);
		
		JLabel lblAntriebU = new JLabel("=");
		lblAntriebU.setForeground(Color.ORANGE);
		lblAntriebU.setFont(new Font("Tahoma", Font.PLAIN, 99));
		lblAntriebU.setBounds(27, 178, 114, 70);
		pnlLehrerAnsicht.add(lblAntriebU);
		
		JLabel lblKopf = new JLabel(">");
		lblKopf.setForeground(Color.BLUE);
		lblKopf.setFont(new Font("Tahoma", Font.PLAIN, 99));
		lblKopf.setBounds(336, 101, 186, 83);
		pnlLehrerAnsicht.add(lblKopf);
		
		JPanel pnlPromocode = new JPanel();
		pnlPromocode.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlPromocode, "Promocode");
		pnlPromocode.setLayout(null);
		
		JLabel lblPromocodeName = new JLabel("\u00B0!\"\u00A7$%&?*':@# PROMOCODE #@:'*?&%$\u00A7\"!\u00B0");
		lblPromocodeName.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromocodeName.setFont(new Font("Algerian", Font.BOLD, 30));
		lblPromocodeName.setBounds(12, 13, 748, 55);
		pnlPromocode.add(lblPromocodeName);
		
		JButton btnZueruck = new JButton("ZUR\u00DCCK");
		btnZueruck.setBackground(new Color(240, 230, 140));
		btnZueruck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cl.show(pnlCenter, "LehrerAnsicht");
			}
		});
		btnZueruck.setFont(new Font("Showcard Gothic", Font.BOLD | Font.ITALIC, 15));
		btnZueruck.setBounds(305, 294, 173, 30);
		pnlPromocode.add(btnZueruck);
		
		lblPromocode = new JLabel("*************************");
		lblPromocode.setBackground(new Color(0, 0, 0));
		lblPromocode.setFont(new Font("Snap ITC", Font.PLAIN, 28));
		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromocode.setBounds(12, 78, 748, 43);
		pnlPromocode.add(lblPromocode);
		
		JButton btn = new JButton("2x IT-$");
		btn.setBackground(new Color(240, 230, 140));
		btn.setFont(new Font("Showcard Gothic", Font.BOLD | Font.ITALIC, 22));
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			generierePromocode(2);
			}
		});
		btn.setBounds(191, 120, 115, 205);
		pnlPromocode.add(btn);
		
		JButton btnNewButton_1 = new JButton("+5 IT-$");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			generierePromocode(5);
			}
		});
		btnNewButton_1.setBackground(new Color(240, 230, 140));
		btnNewButton_1.setFont(new Font("Showcard Gothic", Font.BOLD | Font.ITALIC, 22));
		btnNewButton_1.setBounds(477, 120, 115, 205);
		pnlPromocode.add(btnNewButton_1);
		
		JLabel lblHypnose = new JLabel();
		lblHypnose.setIcon(new ImageIcon("./TEN-COIN/src/bilder/Hypnose.jpg"));
		lblHypnose.setHorizontalAlignment(SwingConstants.CENTER);
		lblHypnose.setBounds(305, 120, 173, 173);
		pnlPromocode.add(lblHypnose);
		
		lblH_HT_L = new JLabel();
		lblH_HT_L.setIcon(new ImageIcon("./TEN-COIN/src/bilder/H.png"));
		lblH_HT_L.setHorizontalAlignment(SwingConstants.CENTER);
		lblH_HT_L.setBounds(12, 150, 140, 125);
		pnlPromocode.add(lblH_HT_L);
		
		lblH_HT_R = new JLabel("");
		lblH_HT_R.setIcon(new ImageIcon("./TEN-COIN/src/bilder/H.png"));
		lblH_HT_R.setHorizontalAlignment(SwingConstants.CENTER);
		lblH_HT_R.setBounds(620, 150, 140, 125);
		pnlPromocode.add(lblH_HT_R);
		
		pnlKlassenArchive = new JPanel();
		pnlKlassenArchive.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlKlassenArchive, "KlassenArchive");
		pnlKlassenArchive.setLayout(null);
		
		klassenAuswahl2 = new JList(cc.getKlassenListe().getKlassenListe());
		klassenAuswahl2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		klassenAuswahl2.setBackground(Color.RED);
		klassenAuswahl2.setForeground(Color.WHITE);
		kAuswahl2 = new JScrollPane(klassenAuswahl2);
		pnlKlassenArchive.add(kAuswahl2);
		kAuswahl2.setBounds(503, 0, 35, 185);
		kAuswahl2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JButton btnKlasseAuswaehlen = new JButton("AUSW\u00C4HLEN");
		btnKlasseAuswaehlen.setBounds(501, 193, 264, 25);
		pnlKlassenArchive.add(btnKlasseAuswaehlen);
		btnKlasseAuswaehlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnKlasseAuswaehlen_Clicked();
			}
		});
		tblSchuelerListe = new JTable(cc.getSchuelerListe("").getSchuelerListe(), SchuelerListe.SPALTENNAMEN);
		tblSchuelerListe.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblSchuelerListe.setFillsViewportHeight(true);
		tblSchuelerListe.setBackground(new Color(240, 230, 140));
		JScrollPane spnSchuelerListe = new JScrollPane(tblSchuelerListe);
		spnSchuelerListe.setBounds(-110, 0, 456, 103);
		pnlKlassenArchive.add(spnSchuelerListe);
		spnSchuelerListe.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		tblGruppenMitglieder = new JTable(cc.getGruppenMitglieder("").getGruppenMitglieder(), GruppenMitglieder.SPALTENNAMEN);
		tblGruppenMitglieder.setFont(new Font("Tahoma", Font.PLAIN, 13));
		tblGruppenMitglieder.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblGruppenMitglieder.setFillsViewportHeight(true);
		tblGruppenMitglieder.setBackground(new Color(240, 230, 140));
		JScrollPane spnGruppenMitglieder = new JScrollPane(tblGruppenMitglieder);
		spnGruppenMitglieder.setBounds(0, 125, 240, 103);
		pnlKlassenArchive.add(spnGruppenMitglieder);
		spnGruppenMitglieder.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		tblVerwendungszwecke = new JTable(cc.getVerwendungszweckListe().getVerwendungszweckListe(), VerwendungszweckListe.SPALTENNAMEN);
		tblVerwendungszwecke.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblVerwendungszwecke.setFillsViewportHeight(true);
		tblVerwendungszwecke.setBackground(new Color(240, 230, 140));
		JScrollPane spnVerwendungszwecke = new JScrollPane(tblVerwendungszwecke);
		spnVerwendungszwecke.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		spnVerwendungszwecke.setBounds(345, 0, 146, 227);
		pnlKlassenArchive.add(spnVerwendungszwecke);
	
		JLabel lbllasse = new JLabel("LASSE");
		lbllasse.setFont(new Font("Tahoma", Font.PLAIN, 70));
		lbllasse.setForeground(Color.RED);
		lbllasse.setBounds(575, 65, 202, 181);
		pnlKlassenArchive.add(lbllasse);
		
		JLabel lblK1 = new JLabel("/");
		lblK1.setFont(new Font("Tahoma", Font.BOLD, 99));
		lblK1.setForeground(Color.RED);
		lblK1.setBounds(532, -17, 255, 109);
		pnlKlassenArchive.add(lblK1);
		
		JLabel lblK2 = new JLabel("\\");
		lblK2.setFont(new Font("Tahoma", Font.BOLD, 99));
		lblK2.setForeground(Color.RED);
		lblK2.setBounds(528, 47, 135, 162);
		pnlKlassenArchive.add(lblK2);
		
		JLabel lblLogo1 = new JLabel();
		lblLogo1.setBounds(660, 0, 117, 109);
		lblLogo1.setIcon(new ImageIcon("./TEN-COIN/src/bilder/Logo1.png"));
		pnlKlassenArchive.add(lblLogo1);
		
		JButton btnEmpfaengerhinzufuegen = new JButton("EMPF\u00C4NGER HINZUF\u00DCGEN");
		btnEmpfaengerhinzufuegen.setBounds(0, 101, 347, 25);
		pnlKlassenArchive.add(btnEmpfaengerhinzufuegen);
		
		JLabel lblR = new JLabel("->");
		lblR.setFont(new Font("Tahoma", Font.BOLD, 60));
		lblR.setForeground(Color.RED);
		lblR.setBounds(501, 231, 75, 55);
		pnlKlassenArchive.add(lblR);
		
		JLabel lblL = new JLabel("<-");
		lblL.setFont(new Font("Tahoma", Font.BOLD, 60));
		lblL.setForeground(Color.RED);
		lblL.setBounds(690, 231, 75, 55);
		pnlKlassenArchive.add(lblL);
		
		JButton btnUeberweisen = new JButton("\u00DCBERWEISEN");
		btnUeberweisen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			if (tfdBetrag.getText().equals("") == false && (cc.istUeberwiesenB(lblEmpfaengerListe.getText(), tfdVerwendungszweck.getText(), Integer.parseInt(tfdBetrag.getText())) || cc.istUeberwiesenG(lblEmpfaengerListe.getText(), Integer.parseInt(tfdBetrag.getText())))) {
			lblEmpfaengerListe.setText("");
			tfdVerwendungszweck.setText("");
			tfdBetrag.setText("");
			lblUFehler.setText("");
			tblSchuelerListe.setModel(new DefaultTableModel(cc.getSchuelerListe(String.valueOf(klassenAuswahl2.getSelectedValue())).getSchuelerListe(), SchuelerListe.SPALTENNAMEN));
			}else {lblUFehler.setText("FEHLSCHLAG");}
			}
		});
		btnUeberweisen.setBounds(575, 250, 115, 25);
		pnlKlassenArchive.add(btnUeberweisen);
		
		JButton btnZurueck1 = new JButton("ZUR\u00DCCK");
		btnZurueck1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cl.show(pnlCenter, "LehrerAnsicht");
			}
		});
		btnZurueck1.setBounds(501, 299, 264, 25);
		pnlKlassenArchive.add(btnZurueck1);
		btnEmpfaengerhinzufuegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblEmpfaengerListe.setText(String.valueOf(tblSchuelerListe.getValueAt(tblSchuelerListe.getSelectedRow(), 0)));
			}
		});
		
		lstGruppenListe = new JList(cc.getGruppenListeK(String.valueOf(klassenAuswahl2.getSelectedValue())).getGruppenListe());
		lstGruppenListe.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lstGruppenListe.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstGruppenListe.setBackground(new Color(240, 230, 140));
		lstGruppenListe.setForeground(Color.BLACK);
		lstGruppenListe.setBounds(238, 125, 109, 103);
		kGruppenListe = new JScrollPane(lstGruppenListe);
		kGruppenListe.setBounds(238, 146, 109, 81);
		kGruppenListe.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnlKlassenArchive.add(kGruppenListe);
		
		JButton btnMitgliederAnzeigen = new JButton("GRUPPENMITGLIEDER ANZEIGEN");
		btnMitgliederAnzeigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnMitgliederAnzeigen_Clicked();
			}
		});
		btnMitgliederAnzeigen.setBounds(0, 225, 239, 25);
		pnlKlassenArchive.add(btnMitgliederAnzeigen);
		
		JButton btnGruppenUeberweisung = new JButton("HINZUF\u00DCGEN");
		btnGruppenUeberweisung.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblEmpfaengerListe.setText(String.valueOf(lstGruppenListe.getSelectedValue()));
			}
		});
		btnGruppenUeberweisung.setBounds(237, 225, 110, 25);
		pnlKlassenArchive.add(btnGruppenUeberweisung);
		
		JButton btnVerwendungszweckAuswählen = new JButton("AUSW\u00C4HLEN");
		btnVerwendungszweckAuswählen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tfdVerwendungszweck.setText(String.valueOf(tblVerwendungszwecke.getValueAt(tblVerwendungszwecke.getSelectedRow(), 0)));
			}
		});
		btnVerwendungszweckAuswählen.setBounds(345, 225, 146, 25);
		pnlKlassenArchive.add(btnVerwendungszweckAuswählen);
		
		JLabel lblVerwendungszweck = new JLabel("VERWENDUNGSZWECK:");
		lblVerwendungszweck.setBounds(0, 299, 138, 25);
		pnlKlassenArchive.add(lblVerwendungszweck);
		
		tfdVerwendungszweck = new JTextField();
		tfdVerwendungszweck.setBounds(140, 299, 247, 25);
		pnlKlassenArchive.add(tfdVerwendungszweck);
		tfdVerwendungszweck.setColumns(10);
		
		JButton btnEinfuegen = new JButton("EINF\u00DCGEN");
		btnEinfuegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Verwendungszweck v = new Verwendungszweck(tfdVerwendungszweck.getText());
				cc.neuerVerwendungszweck(v);
				tblVerwendungszwecke.setModel(new DefaultTableModel(cc.getVerwendungszweckListe().getVerwendungszweckListe(), VerwendungszweckListe.SPALTENNAMEN));
				tfdVerwendungszweck.setText("");
			}
		});
		btnEinfuegen.setBounds(394, 299, 97, 25);
		pnlKlassenArchive.add(btnEinfuegen);
		
		JLabel lblEmpfaenger = new JLabel("EMPF\u00C4NGER:");
		lblEmpfaenger.setBounds(0, 250, 75, 25);
		pnlKlassenArchive.add(lblEmpfaenger);
		
		lblEmpfaengerListe = new JLabel("");
		lblEmpfaengerListe.setBounds(78, 250, 309, 25);
		pnlKlassenArchive.add(lblEmpfaengerListe);
		
		JLabel lblBetrag = new JLabel("BETRAG(AUF VORZEICHEN AUFPASSEN):");
		lblBetrag.setBounds(0, 272, 240, 25);
		pnlKlassenArchive.add(lblBetrag);
		
		tfdBetrag = new JTextField();
		tfdBetrag.setHorizontalAlignment(SwingConstants.CENTER);
		tfdBetrag.setColumns(10);
		tfdBetrag.setBounds(238, 272, 149, 25);
		pnlKlassenArchive.add(tfdBetrag);
		
		lblUFehler = new JLabel("");
		lblUFehler.setForeground(Color.RED);
		lblUFehler.setFont(new Font("Algerian", Font.PLAIN, 16));
		lblUFehler.setHorizontalAlignment(SwingConstants.CENTER);
		lblUFehler.setBounds(575, 276, 115, 16);
		pnlKlassenArchive.add(lblUFehler);
		
		JTextField txtSpalteGruppe = new JTextField("Gruppe");
		txtSpalteGruppe.setEditable(false);
		txtSpalteGruppe.setHorizontalAlignment(SwingConstants.CENTER);
		txtSpalteGruppe.setBounds(238, 125, 108, 22);
		pnlKlassenArchive.add(txtSpalteGruppe);
		
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
		public void run() {
		cc.deleteBenutzer(newuser);
		}
		}));
		
	}
	
		public void btnKlasseErstellen_Clicked(){
		Klasse k = new Klasse(txtVName.getText());
		boolean ergebnis = this.cc.neueKlasse(k);
		txtVName.setText("");
		if (ergebnis == false) {
		lblVMeldung.setText("DIE KLASSE IST SCHON VORHANDEN ODER DER NAME IST ZU LANG(MAX. 4 ZEICHEN)!!!");}
		else {lblVMeldung.setText("DIE KLASSE WURDE ERFOLGREICH ERSTELLT!!!");}
		}
	
		public void btnAnmelden_Clicked() throws NoSuchAlgorithmException, InvalidKeySpecException {
			Passwort p = new Passwort();
			if (cc.istRegistriert(tfdEmail1.getText()).equals("-1") == false && p.getPasswort(txtPasswort1.getText(), this.cc.Passwort(tfdEmail1.getText()))) {
				if(tfdEmail1.getText().endsWith("@oszimt.de")) {cl.show(pnlCenter, "LehrerAnsicht");}
				else {
					user = tfdEmail1.getText();
					zurueckSchuelerAnsicht();
					}	
			}else {
			txtPasswort1.setText("");	
			lblFehler1.setText("ÜBERPRÜFEN SIE BITTE IHRE ANGABEN!");
			m.dateiAnspielen("audio/FalscherCode.mp3");}
		}
		
		public void btnRegistrieren_Clicked() throws NoSuchAlgorithmException, InvalidKeySpecException {
			if (klassenAuswahl1.isSelectionEmpty() && tfdEmail2.getText().endsWith("@oszimt.de") == false) {
			lblFehler2.setText("WÄHLEN SIE BITTE IHRE KLASSE!");	
			}else {
			Random r = new Random();
			Passwort p = new Passwort();
			Benutzer b = new Benutzer();
			if (tfdEmail2.getText().endsWith("@oszimt.de")) {b = new Benutzer(tfdEmail2.getText(), tfdVorname.getText(), tfdName.getText(), p.setPasswort(txtPasswort2.getText()), String.valueOf(22022)); lblFragLehrer.setText("");}
			else {b = new Benutzer(tfdEmail2.getText(), tfdVorname.getText(), tfdName.getText(), p.setPasswort(txtPasswort2.getText()), String.valueOf(r.nextInt(99999)));}
			boolean ergebnis = this.cc.neuerBenutzer(b);	
			if (ergebnis == false) {
			lblFehler2.setText("SIE SIND SCHON REGISTRIERT! MELDEN SIE SICH BITTE AN!");
			}else {
				if (tfdEmail2.getText().endsWith("@oszimt.de")==false) {cc.neueVerbindungBK(tfdEmail2.getText(), String.valueOf(klassenAuswahl1.getSelectedValue())); lblFragLehrer.setText("GEH BITTE ZU DEINEM LEHRER UND FRAG IHN NACH DEINEM CODE!!!");}
				Verwendungszweck v = new Verwendungszweck("Willkommensgeschenk!!!");
				cc.neuerVerwendungszweck(v);
				cc.istUeberwiesenB(tfdEmail2.getText(), v.getVerwendungszweck(), 10);
				newuser=tfdEmail2.getText();
				cl.show(pnlCenter, "Code");
				}
			}
			klassenAuswahl1.clearSelection();
		}
	
		public void btnKlasseAuswaehlen_Clicked() {
			lblEmpfaengerListe.setText("");
			tfdBetrag.setText("");
			tfdVerwendungszweck.setText("");
			tblSchuelerListe.setModel(new DefaultTableModel(cc.getSchuelerListe(String.valueOf(klassenAuswahl2.getSelectedValue())).getSchuelerListe(), SchuelerListe.SPALTENNAMEN));
			lstGruppenListe.setListData(cc.getGruppenListeK(String.valueOf(klassenAuswahl2.getSelectedValue())).getGruppenListe());
		}
		public void btnMitgliederAnzeigen_Clicked() {
			tblGruppenMitglieder.setModel(new DefaultTableModel(cc.getGruppenMitglieder(String.valueOf(lstGruppenListe.getSelectedValue())).getGruppenMitglieder(), GruppenMitglieder.SPALTENNAMEN));
		}
		public void btnNeueGruppe_Clicked() {
			Gruppe g = new Gruppe(txtNeueGruppe.getText());
			if (cc.neueGruppe(g)) {
				lblGruppenMeldungen.setText("GRUPPE WURDE ERFOLGREICH ERSTELLT!");
				cc.neueVerbindungBG(user, txtNeueGruppe.getText());
				cc.neueVerbindungKG(cc.getKlasse(user), txtNeueGruppe.getText());
			}else {lblGruppenMeldungen.setText("GRUPPE SCHON VORHANDEN!");}
			txtNeueGruppe.setText("");
		}
		public void btnPlay_Clicked() {
			Verwendungszweck v = new Verwendungszweck("$Glücksspiel$");
			cc.neuerVerwendungszweck(v);
			Random r = new Random();
			int a[] = {	Integer.parseInt(lblaZahl1.getText()), Integer.parseInt(lblaZahl2.getText()), Integer.parseInt(lblaZahl3.getText())};
			Arrays.sort(a);
			if (cc.getKontostand(user)-a[2]>=0) {
			int s = -1*a[2];
			lblZahl1.setText(String.valueOf(r.nextInt(3)+1));
			lblZahl2.setText(String.valueOf(r.nextInt(3)+1));
			lblZahl3.setText(String.valueOf(r.nextInt(3)+1));
			if (Integer.parseInt(lblZahl1.getText())==Integer.parseInt(lblaZahl1.getText())) {s=s+Integer.parseInt(lblaZahl1.getText());}
			if (Integer.parseInt(lblZahl2.getText())==Integer.parseInt(lblaZahl2.getText())) {s=s+Integer.parseInt(lblaZahl2.getText());}
			if (Integer.parseInt(lblZahl3.getText())==Integer.parseInt(lblaZahl3.getText())) {s=s+Integer.parseInt(lblaZahl3.getText());}
			if (s!=0) {cc.istUeberwiesenB(user, v.getVerwendungszweck(), s);}
			if (s>=0) {lblS.setText("+" + String.valueOf(s)); lblS.setForeground(new Color(0, 100, 0));} else {lblS.setText(String.valueOf(s)); lblS.setForeground(new Color(139, 0, 0));}
			lblKontostand2.setText(String.valueOf(cc.getKontostand(user)) + " IT-$");
			}else {lblkeinGuthaben.setText("LEIDER KEIN GUTHABEN VORHANDEN! ZUERST LERNEN UND DANACH SPIELEN!");}
			}
		public void berechneNote() {
			int s = cc.getKontostand(user);
			pbNotenleiste.setValue(s);
			if (s<56) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(0) +"NP -> Ausfall!!! noch " + String.valueOf(281-s) + " IT-$ zum Bestehen(5NP)");}
			if (s>=56 && s<113) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(1) +"NP -> Ausfall!!! noch " + String.valueOf(281-s) + " IT-$ zum Bestehen(5NP)");}
			if (s>=113 && s<169) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(2) +"NP -> Ausfall!!! noch " + String.valueOf(281-s) + " IT-$ zum Bestehen(5NP)");}
			if (s>=169 && s<225) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(3) +"NP -> Ausfall!!! noch " + String.valueOf(281-s) + " IT-$ zum Bestehen(5NP)");}
			if (s>=225 && s<281) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(4) +"NP -> Ausfall!!! noch " + String.valueOf(281-s) + " IT-$ zum Bestehen(5NP)");}
			if (s>=281 && s<313) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(5) +"NP -> noch " + String.valueOf(313-s) + " IT-$ zu " + String.valueOf(6) + "NP");}
			if (s<313) {lblKNP.setForeground(new Color(139, 0, 0)); lblKNP.setFont(new Font("Algerian", Font.BOLD, 15)); pbNotenleiste.setForeground(new Color(139, 0, 0));}
			if (s>=313) {lblKNP.setFont(new Font("Algerian", Font.BOLD, 20));}
			if (s>=313 && s<344) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(6) +"NP -> noch " + String.valueOf(344-s) + " IT-$ zu " + String.valueOf(7) + "NP");}
			if (s>=344 && s<375) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(7) +"NP -> noch " + String.valueOf(375-s) + " IT-$ zu " + String.valueOf(8) + "NP");}
			if (s>=375 && s<406) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(8) +"NP -> noch " + String.valueOf(406-s) + " IT-$ zu " + String.valueOf(9) + "NP");}
			if (s>=406 && s<438) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(9) +"NP -> noch " + String.valueOf(438-s) + " IT-$ zu " + String.valueOf(10) + "NP");}
			if (s>=438 && s<469) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(10) +"NP -> noch " + String.valueOf(469-s) + " IT-$ zu " + String.valueOf(11) + "NP");}
			if (s>=313 && s<469) {lblKNP.setForeground(new Color(255, 140, 0)); pbNotenleiste.setForeground(new Color(255, 140, 0));}
			if (s>=469 && s<500) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(11) +"NP -> noch " + String.valueOf(500-s) + " IT-$ zu " + String.valueOf(12) + "NP");}
			if (s>=500 && s<531) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(12) +"NP -> noch " + String.valueOf(531-s) + " IT-$ zu " + String.valueOf(13) + "NP");}
			if (s>=531 && s<563) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(13) +"NP -> noch " + String.valueOf(563-s) + " IT-$ zu " + String.valueOf(14) + "NP");}
			if (s>=563 && s<594) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(14) +"NP -> noch " + String.valueOf(594-s) + " IT-$ zu " + String.valueOf(15) + "NP");}
			if (s>=594) {lblKNP.setText(String.valueOf(s) + " IT-$ -> " + String.valueOf(15) +"NP -> Gratulation!!!");}
			if (cc.getKontostand(user)>=469) {lblKNP.setForeground(new Color(0, 100, 0)); pbNotenleiste.setForeground(new Color(0, 100, 0));}
		}
		public void zurueckSchuelerAnsicht() {
			berechneNote();
			tblUeberweisungsListe.setModel(new DefaultTableModel(cc.UeberweisungsListe(user).getUeberweisungsListe(), UeberweisungsListe.SPALTENNAMEN));
			spaltenBreite(tblUeberweisungsListe, spaltenlaengeUL);
			if (cc.getIstErledigt(0)==1) {CheckBox_1.setSelected(true);}else {CheckBox_1.setSelected(false);}
			if (cc.getIstErledigt(1)==1) {CheckBox_2.setSelected(true);}else {CheckBox_2.setSelected(false);}
			if (cc.getIstErledigt(2)==1) {CheckBox_3.setSelected(true);}else {CheckBox_3.setSelected(false);}
			if (cc.getIstErledigt(3)==1) {CheckBox_4.setSelected(true);}else {CheckBox_4.setSelected(false);}
			if (cc.getIstErledigt(4)==1) {CheckBox_5.setSelected(true);}else {CheckBox_5.setSelected(false);}
			if (cc.getIstErledigt(5)==1) {CheckBox_6.setSelected(true);}else {CheckBox_6.setSelected(false);}
			txtPromocodeEingabe.setText("(ZUERST DIE LEISTUNG AUSW\u00C4HLEN)");
			cl.show(pnlCenter, "SchuelerAnsicht");
		}
		public void spaltenBreite(JTable t, int[] b) {
		      int i = 0;
		        for (int breite : b) {
		            TableColumn spalte = t.getColumnModel().getColumn(i++);
		            spalte.setMinWidth(breite);
		            spalte.setMaxWidth(breite);
		            spalte.setPreferredWidth(breite);
		        }
		}
		public void entfernen_umbenennen(int a) {
			nz=0;
			while (String.valueOf(tblSchuelerListeV.getSelectedValue()).charAt(nz)!='|'){
			nz++;
	}
			String em = String.valueOf(tblSchuelerListeV.getSelectedValue()).substring(0, nz);
			
		if (a==0) {btnKlasseErstellen_Clicked();klassenAuswahlV.setListData(cc.getKlassenListe().getKlassenListe());}
		if (a==1) {cc.deleteKlasse(String.valueOf(klassenAuswahlV.getSelectedValue()));klassenAuswahlV.setListData(cc.getKlassenListe().getKlassenListe());}
		if (a==2) {cc.deleteBenutzer(String.valueOf(em));tblSchuelerListeV.setListData(cc.getSchuelerListe("").getSchuelerListeV());}
		if (a==3) {cc.deleteGruppe(String.valueOf(lstGruppenListeV.getSelectedValue()));lstGruppenListeV.setListData(cc.getGruppenListeK(String.valueOf(klassenAuswahlV.getSelectedValue())).getGruppenListe());}
		if (a==4) {cc.deleteMitglied(String.valueOf(lstGruppenMitgliederV.getSelectedValue()), String.valueOf(lstGruppenListeV.getSelectedValue()));tblSchuelerListeV.setListData(cc.getSchuelerListe("").getSchuelerListeV());}
		if (a==5) {cc.deleteVerwendungszweck(String.valueOf(lstVerwendungszweckeV.getSelectedValue()));lstVerwendungszweckeV.setListData(cc.getVerwendungszweckListe().getVerwendungszweckListeV());}
		if (a==6) {if (cc.renameKlasse(String.valueOf(klassenAuswahlV.getSelectedValue()), txtVName.getText()) == false) {lblVMeldung.setText("DIE KLASSE IST SCHON VORHANDEN ODER DER NAME IST ZU LANG(MAX. 4 ZEICHEN)!!!");}klassenAuswahlV.setListData(cc.getKlassenListe().getKlassenListe());}
		if (a==7) {if (cc.renameGruppe(String.valueOf(lstGruppenListeV.getSelectedValue()), txtVName.getText()) == false) {lblVMeldung.setText("DER GRUPPENNAME IST LEIDER SCHON VERGEBEN!!!");}lstGruppenListeV.setListData(cc.getGruppenListeK(String.valueOf(klassenAuswahlV.getSelectedValue())).getGruppenListe());}
		if (a==8) {if (cc.renameVerwendungszweck(String.valueOf(lstVerwendungszweckeV.getSelectedValue()), txtVName.getText()) == false) {lblVMeldung.setText("DIESER VERWENDUNGSZWECK EXISTIERT SCHON!!!");}lstVerwendungszweckeV.setListData(cc.getVerwendungszweckListe().getVerwendungszweckListeV());}
		}
		public void generierePromocode(int a) {
			Random r = new Random();
			long z;
			boolean t = true;
			while (t) {
			z = Math.abs(r.nextLong());
			if (cc.neuerPromocode(z, a)) {
			lblPromocode.setText(String.valueOf(z));
			t=false;
			lblH_HT_L.setIcon(new ImageIcon("./TEN-COIN/src/bilder/HT.png"));
			lblH_HT_R.setIcon(new ImageIcon("./TEN-COIN/src/bilder/HT.png"));
			}	
			}
		}
		public void codeEinloesen() {
		if (tblUeberweisungsListe.isColumnSelected(0) || tblUeberweisungsListe.isColumnSelected(1) || tblUeberweisungsListe.isColumnSelected(2)) {
		int r = tblUeberweisungsListe.getSelectedRow();
		int wert = cc.getWert(Long.parseLong(txtPromocodeEingabe.getText()));
		int newWert = Integer.parseInt(String.valueOf(tblUeberweisungsListe.getValueAt(r, 1)).substring(0, String.valueOf(tblUeberweisungsListe.getValueAt(r, 1)).indexOf(" IT-$")));
		if (wert==0) {txtPromocodeEingabe.setText("PROMOCODE LEIDER UNGÜLTIG!!!");}
		else {
		if (wert==2) {cc.codeEinloesen(user, String.valueOf(tblUeberweisungsListe.getValueAt(r, 0)), String.valueOf(tblUeberweisungsListe.getValueAt(r, 2)).substring(1, String.valueOf(tblUeberweisungsListe.getValueAt(r, 2)).length()-2), newWert*2, Long.parseLong(txtPromocodeEingabe.getText()));}
		else {cc.codeEinloesen(user, String.valueOf(tblUeberweisungsListe.getValueAt(r, 0)), String.valueOf(tblUeberweisungsListe.getValueAt(r, 2)).substring(1, String.valueOf(tblUeberweisungsListe.getValueAt(r, 2)).length()-2), newWert+5, Long.parseLong(txtPromocodeEingabe.getText()));}
		zurueckSchuelerAnsicht();
		txtPromocodeEingabe.setText("(PROMOCODE WURDE EINGELÖST!!!)");
		}
		}else {txtPromocodeEingabe.setText("(ZUERST DIE LEISTUNG AUSW\u00C4HLEN)");}
		}
}