package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Random;

import data.Benutzer;
import data.Gruppe;
import data.GruppenListe;
import data.GruppenMitglieder;
import data.Klasse;
import data.KlassenListe;
import data.SchuelerListe;
import data.Ueberweisung;
import data.UeberweisungsListe;
import data.Verwendungszweck;
import data.VerwendungszweckListe;

public class Database {

	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/ten_coin";
	private String user = "root";
	private String password = "";

	public boolean neuerBenutzer(Benutzer b) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Benutzer VALUES ('" + b.getEmail() + "','"  + b.getVorname() + "','" + b.getName() + "','" +  b.getPasswort() + "','" + b.getCode() + "');";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean deleteBenutzer(String P_Email) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "DELETE FROM T_Benutzer WHERE P_Email = '" + P_Email + "';";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public String setgetCode(String P_Email) {
		String code = "-1";
		Random r = new Random();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			stmt.executeUpdate("UPDATE T_Benutzer SET Code = " + r.nextInt(99999) + " WHERE P_Email = '" + P_Email + "';");
			ResultSet rs = stmt.executeQuery("SELECT Code FROM T_Benutzer WHERE P_Email = '" + P_Email + "';");
			while (rs.next()) {
				code = rs.getString("Code");
			}
			con.close();
		} catch (Exception ex) {
		}
		return code;
	}
	
	public boolean validateCode(String P_Email, int Code) {
		int code = 0;
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM T_Benutzer WHERE P_Email = '" + P_Email + "' AND Code = " + Code + ";");
			while (rs.next()) {
				code = rs.getInt("COUNT(*)");
				if (code>0) {return true;}
			}
			con.close();
		} catch (Exception ex) {
		return false;
		}
		return false;
	}

	public boolean neuerVerwendungszweck(Verwendungszweck v) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Verwendungszwecke VALUES ('" + v.getVerwendungszweck() + "');";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean renameVerwendungszweck(String P_Verwendungszweck_Alt, String P_Verwendungszweck_Neu) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "UPDATE T_Verwendungszwecke SET P_Verwendungszweck = '" + P_Verwendungszweck_Neu + "' WHERE P_Verwendungszweck = '" + P_Verwendungszweck_Alt + "';";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean deleteVerwendungszweck(String P_Verwendungszweck) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "DELETE FROM T_Verwendungszwecke WHERE P_Verwendungszweck = '" + P_Verwendungszweck + "';";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean neueKlasse(Klasse k) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Klassen VALUES ('" + k.getKlasse() + "');";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean renameKlasse(String P_Klasse_Alt, String P_Klasse_Neu) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "UPDATE T_Klassen SET P_Klasse = '" + P_Klasse_Neu + "' WHERE P_Klasse = '" + P_Klasse_Alt + "';";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean deleteKlasse(String P_Klasse) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "DELETE FROM T_Klassen WHERE P_Klasse = '" + P_Klasse + "';";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public String getKlasse(String PF_Email) {
		String klasse = "-1";
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT PF_Klasse FROM T_Benutzer_Klassen WHERE PF_Email = '" + PF_Email + "';";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				klasse = rs.getString("PF_Klasse");
			}
			con.close();
		} catch (Exception ex) {
		}
		return klasse;
	}
	
	public boolean neueGruppe(Gruppe g) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Gruppen VALUES ('" + g.getGruppe() + "');";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean renameGruppe(String P_Gruppe_Alt, String P_Gruppe_Neu) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "UPDATE T_Gruppen SET P_Gruppe = '" + P_Gruppe_Neu + "' WHERE P_Gruppe = '" + P_Gruppe_Alt + "';";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean deleteGruppe(String P_Gruppe) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "DELETE FROM T_Gruppen WHERE P_Gruppe = '" + P_Gruppe + "';";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean deleteMitglied(String PF_Email, String PF_Gruppe) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "DELETE FROM T_Benutzer_Gruppen WHERE PF_Email = '" + PF_Email + "' AND PF_Gruppe = '" + PF_Gruppe + "';";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean neueVerbindungBK(String email, String klasse) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Benutzer_Klassen VALUES ('" + email + "','" + klasse + "');";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean neueVerbindungBUe(String email, int id) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Benutzer_Überweisungen VALUES ('" + email + "','" + id + "');";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean neueVerbindungBG(String email, String gruppe) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Benutzer_Gruppen VALUES ('" + email + "','" + gruppe + "');";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean neueVerbindungKG(String klasse, String gruppe) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Klassen_Gruppen VALUES ('" + klasse + "','" + gruppe + "');";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public String istRegistriert(String P_Email) {
		String email = "-1";
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT P_Email FROM T_Benutzer WHERE P_Email = '" + P_Email + "';";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				email = rs.getString("P_Email");
			}
			con.close();
		} catch (Exception ex) {
		}
		return email;
	}
	
	public boolean istUeberwiesenB(String PF_Email, String PF_Verwendungszweck , int Betrag) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Benutzer_Überweisungen VALUES ('" + PF_Email + "','" + PF_Verwendungszweck + "'," + Betrag + ",CURRENT_TIMESTAMP);";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean istUeberwiesenG(String PF_Gruppe, int Betrag) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Gruppen_Überweisungen VALUES ('" + PF_Gruppe + "'," + Betrag + ",CURRENT_TIMESTAMP);";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public int getKontostand(String PF_Email) {
		int kontostand = 0;
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT SUM(Betrag) FROM T_Benutzer_Überweisungen WHERE PF_Email='" + PF_Email + "';";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				kontostand = rs.getInt("SUM(Betrag)");
			}
			con.close();
		} catch (Exception ex) {
		}
		return kontostand;
	}
	
	public int getGruppenkasse(String PF_Gruppe) {
		int gruppenkasse = 0;
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT SUM(Betrag) FROM T_Gruppen_Überweisungen WHERE PF_Gruppe='" + PF_Gruppe + "';";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				gruppenkasse = rs.getInt("SUM(Betrag)");
			}
			con.close();
		} catch (Exception ex) {
		}
		return gruppenkasse;
	}

	public String Passwort(String P_Email) {
		String passwort = null;
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT Passwort FROM T_Benutzer WHERE P_Email = '" + P_Email + "';";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				passwort = rs.getString("Passwort");
			}
			con.close();
		} catch (Exception ex) {
		}
		return passwort;
	}
	
	public boolean neuesPasswort(String P_Email, String Passwort) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "UPDATE T_Benutzer SET Passwort = '" + Passwort + "' WHERE P_Email = '" + P_Email + "';";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
		return false;
		}
		return true;
	}
	
	public SchuelerListe getSchuelerListe(String klasse) {
		SchuelerListe schuelerListe = new SchuelerListe();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT * FROM T_Benutzer WHERE P_Email IN (SELECT PF_Email FROM t_Benutzer_Klassen WHERE PF_Klasse =  '" + klasse + "')  ORDER BY Name;";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Benutzer b = new Benutzer(rs.getString("P_Email"), rs.getString("Vorname"), rs.getString("Name"), rs.getString("Passwort"), rs.getString("Code"));
				schuelerListe.addSchuelerListe(b);
			}
			con.close();
		} catch (Exception ex) {
		}
		return schuelerListe;
	}
	
	public KlassenListe getKlassenListe() {
		KlassenListe klassenListe = new KlassenListe();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT * FROM T_Klassen ORDER BY P_Klasse;";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Klasse k = new Klasse(rs.getString("P_Klasse"));
				klassenListe.addKlassenListe(k);
			}
			con.close();
		} catch (Exception ex) {
		}
		return klassenListe;
	}
	
	public GruppenListe getGruppenListeK(String klasse) {
		GruppenListe gruppenListe = new GruppenListe();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT DISTINCT PF_Gruppe FROM T_Klassen_Gruppen WHERE PF_Klasse = '" + klasse + "' ORDER BY PF_Gruppe;";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Gruppe g = new Gruppe(rs.getString("PF_Gruppe"));
				gruppenListe.addGruppenListe(g);
			}
			con.close();
		} catch (Exception ex) {
		}
		return gruppenListe;
	}
	
	public GruppenListe getGruppenListeB(String PF_Email) {
		GruppenListe gruppenListe = new GruppenListe();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT PF_Gruppe FROM T_Benutzer_Gruppen WHERE PF_Email = '" + PF_Email + "' ORDER BY PF_Gruppe;";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Gruppe g = new Gruppe(rs.getString("PF_Gruppe"));
				gruppenListe.addGruppenListe(g);
			}
			con.close();
		} catch (Exception ex) {
		}
		return gruppenListe;
	}
	
	public GruppenMitglieder getGruppenMitglieder(String gruppe) {
		GruppenMitglieder gruppenMitglieder = new GruppenMitglieder();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT * FROM T_Benutzer WHERE P_Email IN (SELECT PF_Email FROM t_Benutzer_Gruppen WHERE PF_Gruppe =  '" + gruppe + "')  ORDER BY Name;";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Benutzer b = new Benutzer(rs.getString("P_Email"), rs.getString("Vorname"), rs.getString("Name"), rs.getString("Passwort"), rs.getString("Code"));
				gruppenMitglieder.addGruppenMitglieder(b);
			}
			con.close();
		} catch (Exception ex) {
		}
		return gruppenMitglieder;
	}
	
	public VerwendungszweckListe getVerwendungszweckListe() {
		VerwendungszweckListe verwendungszweckListe = new VerwendungszweckListe();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT * FROM T_Verwendungszwecke ORDER BY P_Verwendungszweck;";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Verwendungszweck v = new Verwendungszweck(rs.getString("P_Verwendungszweck"));
				verwendungszweckListe.addVerwendungszweckListe(v);
			}
			con.close();
		} catch (Exception ex) {
		}
		return verwendungszweckListe;
	}
	
	public UeberweisungsListe getUeberweisungsListe(String PF_Email) {
		UeberweisungsListe ueberweisungsListe = new UeberweisungsListe();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT * FROM T_Benutzer_Überweisungen WHERE PF_Email = '" + PF_Email + "' ORDER BY Zeitpunkt DESC;";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Ueberweisung u = new Ueberweisung(rs.getString("PF_Verwendungszweck"), rs.getInt("Betrag"), rs.getString("Zeitpunkt"));
				ueberweisungsListe.addUeberweisungsListe(u);
			}
			con.close();
		} catch (Exception ex) {
		}
		return ueberweisungsListe;
	}
	
	public boolean setIstErledigt(int P_ID, int istErledigt) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "UPDATE T_AT SET istErledigt = " + istErledigt + " WHERE P_ID = " + P_ID + ";";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
		return false;
		}
		return true;
	}
	
	public int getIstErledigt(int P_ID) {
		int istErledigt = 0;
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT istErledigt FROM T_AT WHERE P_ID = " + P_ID + ";";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				istErledigt = rs.getInt("istErledigt");
			}
			con.close();
		} catch (Exception ex) {
		}
		return istErledigt;
	}
	
	public boolean neuerPromocode(long P_Promocode, int Wert) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Promocodes VALUES (" + P_Promocode + "," + Wert + ");";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public int getWert(long P_Promocode) {
		int wert = 0;
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT Wert FROM T_Promocodes WHERE P_Promocode = " + P_Promocode + ";";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				wert = rs.getInt("Wert");
			}
			con.close();
		} catch (Exception ex) {
		}
		return wert;
	}
	
	public boolean codeEinloesen(String PF_Email, String PF_Verwendungszweck, String Zeitpunkt, int Wert, long P_Promocode) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql1 = "UPDATE T_Benutzer_Überweisungen SET Betrag = " + Wert + " WHERE PF_Email = '" + PF_Email + "' AND PF_Verwendungszweck = '" + PF_Verwendungszweck + "' AND Zeitpunkt = '" + Zeitpunkt + "';";
			String sql2 = "UPDATE T_Promocodes SET Wert = 0 WHERE P_Promocode = " + P_Promocode + ";";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql1);
			stmt.executeUpdate(sql2);
			con.close();
		} catch (Exception ex) {
		return false;
		}
		return true;
	}
	
	public void setup() {
		try {
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/?";
			String user = "root";
			String password = "";
			Class.forName(driver);
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS ten_coin;");
			con.close();
			url = "jdbc:mysql://localhost/ten_coin?";
			con = DriverManager.getConnection(url, user, password);
			stmt = con.createStatement();
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Benutzer (" + "P_Email VARCHAR(60) PRIMARY KEY," + "Vorname VARCHAR(25)," + "Name VARCHAR(25)," + "Passwort VARCHAR(1024)," + "Code CHAR(5));");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Klassen (" + "P_Klasse CHAR(4) PRIMARY KEY);");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Gruppen (" + "P_Gruppe VARCHAR(50) PRIMARY KEY);");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Verwendungszwecke (" + "P_Verwendungszweck VARCHAR(100) PRIMARY KEY);");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_AT (" + "P_ID INT PRIMARY KEY," + " istErledigt INT);");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Promocodes (" + "P_Promocode BIGINT PRIMARY KEY," + "Wert INT);");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Benutzer_Klassen (" + "PF_Email VARCHAR(60)," + "PF_Klasse CHAR(4)," + "CONSTRAINT pk_T_Benutzer_Klassen PRIMARY KEY(PF_Email, PF_Klasse)," + "CONSTRAINT fk_T_Benutzer_Klassen_1 FOREIGN KEY(PF_Email) REFERENCES T_Benutzer(P_Email) ON DELETE CASCADE ON UPDATE CASCADE," + "CONSTRAINT fk_T_Benutzer_Klassen_2 FOREIGN KEY(PF_Klasse) REFERENCES T_Klassen(P_Klasse) ON DELETE CASCADE ON UPDATE CASCADE);");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Benutzer_Gruppen (" + "PF_Email VARCHAR(60)," + "PF_Gruppe VARCHAR(50)," + "CONSTRAINT pk_T_Benutzer_Gruppen PRIMARY KEY(PF_Email, PF_Gruppe)," + "CONSTRAINT fk_T_Benutzer_Gruppen_1 FOREIGN KEY(PF_Email) REFERENCES T_Benutzer(P_Email) ON DELETE CASCADE ON UPDATE CASCADE," + "CONSTRAINT fk_T_Benutzer_Gruppen_2 FOREIGN KEY(PF_Gruppe) REFERENCES T_Gruppen(P_Gruppe) ON DELETE CASCADE ON UPDATE CASCADE);");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Benutzer_Überweisungen (" + "PF_Email VARCHAR(60)," + "PF_Verwendungszweck VARCHAR(100)," + "Betrag INT," + "Zeitpunkt DATETIME," + "CONSTRAINT pk_T_Benutzer_Überweisungen PRIMARY KEY(PF_Email, PF_Verwendungszweck, Zeitpunkt)," + "CONSTRAINT fk_T_Benutzer_Überweisungen_1 FOREIGN KEY(PF_Email) REFERENCES T_Benutzer(P_Email) ON DELETE CASCADE ON UPDATE CASCADE," + "CONSTRAINT fk_T_Benutzer_Überweisungen_2 FOREIGN KEY(PF_Verwendungszweck) REFERENCES T_Verwendungszwecke(P_Verwendungszweck) ON DELETE CASCADE ON UPDATE CASCADE);");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Gruppen_Überweisungen (" + "PF_Gruppe VARCHAR(50)," + "Betrag INT," + "Zeitpunkt DATETIME," + "CONSTRAINT pk_T_Gruppen_Überweisungen PRIMARY KEY(PF_Gruppe, Zeitpunkt)," + "CONSTRAINT fk_T_Gruppen_Überweisungen_1 FOREIGN KEY(PF_Gruppe) REFERENCES T_Gruppen(P_Gruppe) ON DELETE CASCADE ON UPDATE CASCADE);");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Klassen_Gruppen (" + "PF_Klasse CHAR(4)," + "PF_Gruppe VARCHAR(50)," + "CONSTRAINT pk_T_Klassen_Gruppen PRIMARY KEY(PF_Klasse, PF_Gruppe)," + "CONSTRAINT fk_T_Klassen_Gruppen_1 FOREIGN KEY(PF_Klasse) REFERENCES T_Klassen(P_Klasse) ON DELETE CASCADE ON UPDATE CASCADE," + "CONSTRAINT fk_T_Klassen_Gruppen_2 FOREIGN KEY(PF_Gruppe) REFERENCES T_Gruppen(P_Gruppe) ON DELETE CASCADE ON UPDATE CASCADE);");
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM T_AT;");
			boolean insert = true;
			if (rs.next())
				insert = rs.getInt(1) == 0;
			if (insert) {
			stmt.executeUpdate("INSERT INTO T_AT VALUES (0,0);");
			stmt.executeUpdate("INSERT INTO T_AT VALUES (1,0);");
			stmt.executeUpdate("INSERT INTO T_AT VALUES (2,0);");
			stmt.executeUpdate("INSERT INTO T_AT VALUES (3,0);");
			stmt.executeUpdate("INSERT INTO T_AT VALUES (4,0);");
			stmt.executeUpdate("INSERT INTO T_AT VALUES (5,0);");}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}