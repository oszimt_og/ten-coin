package logic;

import data.Benutzer;
import data.Gruppe;
import data.GruppenListe;
import data.GruppenMitglieder;
import data.Klasse;
import data.KlassenListe;
import data.SchuelerListe;
import data.UeberweisungsListe;
import data.Verwendungszweck;
import data.VerwendungszweckListe;
import database.Database;

public class Controller {

	private Database db;

	public Controller() {
		this.db = new Database();
		db.setup();
	}

	public boolean neuerVerwendungszweck(Verwendungszweck v) {
		return db.neuerVerwendungszweck(v);
	}
	
	public boolean renameVerwendungszweck(String P_Verwendungszweck_Alt, String P_Verwendungszweck_Neu) {
		return db.renameVerwendungszweck(P_Verwendungszweck_Alt, P_Verwendungszweck_Neu);
	}
	
	public boolean deleteVerwendungszweck(String P_Verwendungszweck) {
		return db.deleteVerwendungszweck(P_Verwendungszweck);
	}

	public boolean neuerBenutzer(Benutzer b) {
		return db.neuerBenutzer(b);
	}
	
	public boolean deleteBenutzer(String P_Email) {
		return db.deleteBenutzer(P_Email);
	}
	
	public String setgetCode(String P_Email) {
		return db.setgetCode(P_Email);
	}
	
	public boolean validateCode(String P_Email, int Code) {
		return db.validateCode(P_Email, Code);
	}

	public boolean neueKlasse(Klasse k) {
		return db.neueKlasse(k);
	}
	
	public boolean renameKlasse(String P_Klasse_Alt, String P_Klasse_Neu) {
		return db.renameKlasse(P_Klasse_Alt, P_Klasse_Neu);
	}
	
	public boolean deleteKlasse(String P_Klasse) {
		return db.deleteKlasse(P_Klasse);
	}
	
	public String getKlasse(String email) {
		return db.getKlasse(email);
	}
	
	public boolean neueGruppe(Gruppe g) {
		return db.neueGruppe(g);
	}
	
	public boolean renameGruppe(String P_Gruppe_Alt, String P_Gruppe_Neu) {
		return db.renameGruppe(P_Gruppe_Alt, P_Gruppe_Neu);
	}
	
	public boolean deleteGruppe(String P_Gruppe) {
		return db.deleteGruppe(P_Gruppe);
	}
	
	public boolean deleteMitglied(String PF_Email, String PF_Gruppe) {
		return db.deleteMitglied(PF_Email, PF_Gruppe);
	}
	
	public String istRegistriert(String P_Email) {
		return db.istRegistriert(P_Email);
	}

	public String Passwort(String P_Email) {
		return db.Passwort(P_Email);
	}
	
	public boolean neuesPasswort(String P_Email, String Passwort) {
		return db.neuesPasswort(P_Email, Passwort);
	}

	public SchuelerListe getSchuelerListe(String klasse) {
		return db.getSchuelerListe(klasse);
	}

	public KlassenListe getKlassenListe() {
		return db.getKlassenListe();
	}
	
	public GruppenListe getGruppenListeK(String klasse) {
		return db.getGruppenListeK(klasse);
	}
	
	public GruppenListe getGruppenListeB(String email) {
		return db.getGruppenListeB(email);
	}
	
	public GruppenMitglieder getGruppenMitglieder(String gruppe) {
		return db.getGruppenMitglieder(gruppe);
	}
	
	public VerwendungszweckListe getVerwendungszweckListe() {
		return db.getVerwendungszweckListe();
	}
	
	public UeberweisungsListe UeberweisungsListe(String PF_Email) {
		return db.getUeberweisungsListe(PF_Email);
	}
	
	public boolean istUeberwiesenB(String PF_Email, String PF_Verwendungszweck, int Betrag) {
		return db.istUeberwiesenB(PF_Email, PF_Verwendungszweck, Betrag);
	}
	
	public boolean istUeberwiesenG(String PF_Gruppe, int Betrag) {
		return db.istUeberwiesenG(PF_Gruppe, Betrag);
	}
	
	public int getKontostand(String PF_Email) {
		return db.getKontostand(PF_Email);
	}
	
	public int getGruppenkasse(String PF_Gruppe) {
		return db.getGruppenkasse(PF_Gruppe);
	}
	
	public boolean neueVerbindungBK(String email, String klasse) {
		return db.neueVerbindungBK(email, klasse);
	}
	
	public boolean neueVerbindungBUe(String email, int id) {
		return db.neueVerbindungBUe(email, id);
	}
	
	public boolean neueVerbindungBG(String email, String gruppe) {
		return db.neueVerbindungBG(email, gruppe);
	}
	
	public boolean neueVerbindungKG(String klasse, String gruppe) {
		return db.neueVerbindungKG(klasse, gruppe);
	}
	
	public boolean setIstErledigt(int P_ID, int istErledigt) {
		return db.setIstErledigt(P_ID, istErledigt);
	}
	
	public int getIstErledigt(int P_ID) {
		return db.getIstErledigt(P_ID);
	}
	
	public boolean neuerPromocode(long P_Promocode, int Wert) {
		return db.neuerPromocode(P_Promocode, Wert);
	}
	
	public int getWert(long P_Promocode) {
		return db.getWert(P_Promocode);
	}
	
	public boolean codeEinloesen(String PF_Email, String PF_Verwendungszweck, String Zeitpunkt, int Wert, long P_Promocode) {
		return db.codeEinloesen(PF_Email, PF_Verwendungszweck, Zeitpunkt, Wert, P_Promocode);
	}
}
