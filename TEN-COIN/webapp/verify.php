<?php
session_start();

require_once("model.php");
require_once("config.php");
require_once("functions.php");

$logged_in = False;
$is_lehrer = False;

if(isset($_SESSION["user_email"])) {
    $logged_in = True;
    $user_data = getUserByEmail($mysql, $_SESSION["user_email"]);
    $is_lehrer = isLehrer($user_data->P_Email);
}


if($is_lehrer || !$logged_in || $user_data->Code == 0) {
    header("Location: /");
    die();
}


$success = False;
$error = False;


if(isset($_POST["code"], $_POST["g-recaptcha-response"])) {
    if($_POST["code"] == $user_data->Code && verifyRecaptcha($_POST["g-recaptcha-response"], getUserIp())) {
        updateCode($mysql, $user_data->P_Email, 0);
        header("Location: /");
        die();
    } else {
        $error = "Bitte überprüfe deine Eingabe.";
    }
}


?>

<html>
    <head>
        <!-- CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <title>TenCoin - Glücksspiel</title>
        
    </head>

    <body>

         <div class="container">

             
            <br>

            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="/">TenCoin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home
                            <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <?php if($logged_in): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/gluecksspiel">Glücksspiel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/gruppen">Gruppen</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Abmelden</a>
                        </li>
                        <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Anmelden</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Registrieren</a>
                        </li>
                        <?php endif; ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/datenschutz">Datenschutzerklärung</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/impressum">Impressum</a>
                        </li>
                    </ul>

                </div>
            </nav>

            <br>
        


            <div class="jumbotron">
                <h1 class="display-3">Hey, <?= $user_data->Vorname  ?>!</h1>
                <hr class="my-4">
                <h3>Um diese Seite nutzen zu können, verifiziere Dich bitte. <br>Frage dazu bitte Deine Lehrer nach einem Code.</h3>
                <hr>
                <?php if($success): ?>
                <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?= $success ?>
                </div>
                <?php endif; ?>
                <?php if($error): ?>
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?= $error ?>
                </div>
                <?php endif; ?>
                <br>
                <form action="/verify" method="post">
                    <div class="form-group">
                        <label>Verifizierungs-Code</label>
                        <input required name="code"  type="number" class="form-control" placeholder="Code eingeben">
                    </div>

                    <div class="g-recaptcha" data-sitekey="6Lf9M-4ZAAAAAAqO8XkNsfULpg702tI56rI6ywG8"></div>

                    <br>
                    <button type="submit" class="btn btn-success btn-lg btn-block">Jetzt verifizieren!</button>
               </form>
                <hr>

            </div>



            <?php require_once("footer.php"); ?>

        </div>

        <!-- jQuery and JS bundle w/ Popper.js -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        
        <!-- Google Recaptcha v2 -->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    </body>
</html>