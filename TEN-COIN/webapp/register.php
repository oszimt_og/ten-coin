<?php

    session_start();


    if(isset($_SESSION["user_email"])) {
        header("Location: /");
        die();
    }

    require_once("model.php");
    require_once("config.php");
    require_once("functions.php");

    $error = False;

    if(isset($_POST["vorname"], $_POST["nachname"], $_POST["email"], $_POST["pass"], $_POST["repeatpass"], $_POST["class"], $_POST["g-recaptcha-response"])):
        $vorname = htmlentities($_POST["vorname"], ENT_QUOTES, 'UTF-8');
        $nachname = htmlentities($_POST["nachname"], ENT_QUOTES, 'UTF-8');
        $email = htmlentities($_POST["email"], ENT_QUOTES, 'UTF-8');
        $pass = htmlentities($_POST["pass"], ENT_QUOTES, 'UTF-8');
        $repeatpass = htmlentities($_POST["repeatpass"], ENT_QUOTES, 'UTF-8');
        $hashedPassword = password_hash($pass, PASSWORD_DEFAULT);
        $class = htmlentities($_POST["class"], ENT_QUOTES, 'UTF-8');

        if(strlen($vorname) < 2):
            $error = "Bitte gebe deinen korrekten Vornamen an.";
        elseif(strlen($nachname) < 2):
            $error = "Bitte gebe deinen korrekten Nachnamen an.";
        elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)):
            $error = "Bitte gebe eine korrekte Email Adresse an.";
        elseif($pass !== $repeatpass):
            $error = "Bitte stelle sicher, dass deine Passwörter übereinstimmen.";
        elseif(!insertUser($mysql, $email, $vorname, $nachname, $hashedPassword, generateCode())):
            $error = "Du bist bereits registriert.";
        elseif(!verifyRecaptcha($_POST["g-recaptcha-response"], getUserIp())):
            $error = "Bitte bestätige, dass du kein Roboter bist.";
        else:
            if(!isLehrer($email)) {
                insertUserKlasse($mysql, $email, $class);
            }
            $_SESSION["user_email"] = $email;
            header("Location: /");
        endif;

    endif;
?>

<html>
    <head>
        <!-- CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <title>TenCoin - Registrierung</title>
        
    </head>

    <body>

         <div class="container">
             
            <br>

            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="/">TenCoin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home
                            <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Anmelden</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Registrieren</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/datenschutz">Datenschutzerklärung</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/impressum">Impressum</a>
                        </li>
                    </ul>

                </div>
            </nav>

            <br>

            <h1>Registrierung</h1>

            <?php if($error): ?>
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?= $error ?>
                </div>
            <?php endif; ?>
        
            <form action="register" method="post">
                <fieldset>

                    <div class="form-group">
                        <label>Vorname</label>
                        <input required name="vorname" type="text" class="form-control" placeholder="Vorname">
                    </div>

                    <div class="form-group">
                        <label>Nachname</label>
                        <input required name="nachname" type="text" class="form-control" placeholder="Nachname">
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input required name="email"  type="email" class="form-control" placeholder="Email">
                        <small id="emailHelp" class="form-text text-muted">Wir werden deine Email-Adresse niemals mit jemandem außer deiner Lehrer teilen.</small>
                    </div>

                    <div class="form-group">
                        <label>Passwort</label>
                        <input required name="pass" type="password" class="form-control" placeholder="Passwort">
                    </div>

                    <div class="form-group">
                        <label>Password wiederholen</label>
                        <input required name="repeatpass" type="password" class="form-control" placeholder="Passwort">
                    </div>

                    <div class="form-group">
                        <label for="exampleSelect1">Klasse auswählen</label>
                        <select required name="class" class="form-control" id="exampleSelect1">
                        <?php foreach (getKlassen($mysql) as $key => $klasse): ?>
                            <option text="<?= $klasse->P_Klasse ?>"><?= $klasse->P_Klasse ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="custom-control custom-switch">
                        <input required type="checkbox" class="custom-control-input" id="customSwitch2">
                        <label class="custom-control-label" for="customSwitch2">Ich bin mit der <a target="_blank" href="/datenschutz">Datenschutzerklärung</a> einverstanden.</label>
                        </div>
                    </div>


                    <div class="g-recaptcha" data-sitekey="6Lf9M-4ZAAAAAAqO8XkNsfULpg702tI56rI6ywG8"></div>

                    <br>

                    <button type="submit" class="btn btn-primary">Registrieren</button>
                </fieldset>
                </form>

                <?php require_once("footer.php"); ?>

        </div>

        <!-- jQuery and JS bundle w/ Popper.js -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

        <!-- Google Recaptcha v2 -->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    </body>
</html>