<?php

require_once("db.php");

/*
 * Überprüfen, ob die Email bereits verwendet wurde.
 */

function isEmailRegistered($mysql, $email) {
    $handle = $mysql->prepare('SELECT COUNT(*) AS "count" FROM `t_benutzer` WHERE P_Email = ?');

    $handle->bindValue(1, $email);

    if($handle->execute()) {
        if($handle->fetch(PDO::FETCH_OBJ)->count == "1") {
            return true;
        }
    }
    return false;
}

/*
 * Nutzer in die Datenbank einfügen
 */

function insertUser($mysql, $email, $vorname, $nachname, $passwort, $code) {
    $handle = $mysql->prepare('INSERT INTO `t_benutzer` (`P_Email`, `Vorname`, `Name`, `Passwort`, `Code`) 
    VALUES (?, ?, ?, ?, ?);');
 
    $handle->bindValue(1, $email);
    $handle->bindValue(2, $vorname);
    $handle->bindValue(3, $nachname);
    $handle->bindValue(4, $passwort);
    $handle->bindValue(5, $code);
    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

/*
 * Nutzerdaten durch Email ausgeben
 */

function getUserByEmail($mysql, $email) {
    $handle = $mysql->prepare('SELECT * FROM `t_benutzer` WHERE P_Email = ?');

    $handle->bindValue(1, $email);

    if($handle->execute()) {
        return $handle->fetch(PDO::FETCH_OBJ);
    }
    return false;
}

/*
 * Kontostand des Nutzers ausgeben
 */

function getTotalOfUser($mysql, $email) {
    $handle = $mysql->prepare('SELECT IFNULL(SUM(Betrag), 0) AS "total" FROM `t_benutzer_überweisungen` WHERE PF_Email = ?');

    $handle->bindValue(1, $email);

    if($handle->execute()) {
        return $handle->fetch(PDO::FETCH_OBJ)->total;
    }
    return false;
}

function getKlassen($mysql) {
    $handle = $mysql->prepare('SELECT * FROM `t_klassen`');

    if($handle->execute()) {
        return $handle->fetchAll(PDO::FETCH_OBJ);
    }
    return false;
}

function insertUserKlasse($mysql, $email, $klasse) {
    $handle = $mysql->prepare('INSERT INTO `t_benutzer_klassen` (`PF_Email`, `PF_Klasse`) VALUES (?, ?);');

    $handle->bindValue(1, $email);
    $handle->bindValue(2, $klasse);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

function getKlasseOfUser($mysql, $email) {
    $handle = $mysql->prepare('SELECT DISTINCT * FROM `t_benutzer_klassen` WHERE PF_Email = ?');
    
    $handle->bindValue(1, $email);
    
    if($handle->execute()) {
        return $handle->fetch(PDO::FETCH_OBJ)->PF_Klasse;
    }
    return false;
}

function getVerwendungszwecke($mysql) {
    $handle = $mysql->prepare('SELECT * FROM `t_überweisungen`');

    if($handle->execute()) {
        return $handle->fetchAll(PDO::FETCH_OBJ);
    }
    return false;
}

function insertVerwendungszweck($mysql, $zweck) {
    $handle = $mysql->prepare('INSERT INTO `t_überweisungen` (`P_Verwendungszweck`) VALUES (?);');

    $handle->bindValue(1, $zweck);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

function insertUeberweisung($mysql, $email, $zweck, $betrag) {
    $handle = $mysql->prepare('INSERT INTO `t_benutzer_überweisungen` (`PF_Email`, `PF_Verwendungszweck`, `Betrag`, `Zeitpunkt`) VALUES (?, ?, ?, CURRENT_TIMESTAMP);');
 
    $handle->bindValue(1, $email);
    $handle->bindValue(2, $zweck);
    $handle->bindValue(3, $betrag);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

function getUeberweisungenByUser($mysql, $email) {
    $handle = $mysql->prepare('SELECT * FROM `t_benutzer_überweisungen` WHERE PF_Email = ? ORDER BY Zeitpunkt DESC');

    $handle->bindValue(1, $email);

    if($handle->execute()) {
        return $handle->fetchAll(PDO::FETCH_OBJ);
    }
    return false;
}

function getUserByKlasse($mysql, $klasse) {
    $handle = $mysql->prepare('SELECT b.*, IFNULL(SUM(Betrag), 0) AS Total FROM `t_benutzer` b left JOIN t_benutzer_überweisungen bü ON b.P_Email = bü.PF_Email right JOIN t_benutzer_klassen bk ON bk.PF_Email = b.P_Email WHERE bk.PF_Klasse = ? GROUP BY b.P_Email ORDER BY Total DESC ');

    $handle->bindValue(1, $klasse);

    if($handle->execute()) {
        return $handle->fetchAll(PDO::FETCH_OBJ);
    }
    return false;
}

function getKlassenByLehrer($mysql, $lehrer) {
    $handle = $mysql->prepare('SELECT * FROM `t_benutzer_klassen` WHERE PF_Email = ?');

    $handle->bindValue(1, $lehrer);

    if($handle->execute()) {
        return $handle->fetchAll(PDO::FETCH_OBJ);
    }
    return false;
}

function insertKlasse($mysql, $klasse) {
    $handle = $mysql->prepare('INSERT INTO `t_klassen` (`P_Klasse`) VALUES (?);');
 
    $handle->bindValue(1, $klasse);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

function insertGruppe($mysql, $gruppe) {
    $handle = $mysql->prepare('INSERT INTO `t_gruppen` (`P_Gruppe`, `Gruppenkasse`) VALUES (?, 0);');
 
    $handle->bindValue(1, $gruppe);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

function addUserToGruppe($mysql, $gruppe, $email) {
    $handle = $mysql->prepare('INSERT INTO `t_benutzer_gruppen` (`PF_Email`, `PF_Gruppe`) VALUES (?, ?);');
 
    $handle->bindValue(1, $email);
    $handle->bindValue(2, $gruppe);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

function getGruppenByUser($mysql, $email) {
    $handle = $mysql->prepare('SELECT DISTINCT PF_Gruppe FROM `t_benutzer_gruppen` WHERE PF_Email = ?');

    $handle->bindValue(1, $email);

    if($handle->execute()) {
        return $handle->fetchAll(PDO::FETCH_OBJ);
    }
    return false;
}

function isUserInGruppe($mysql, $email, $gruppe) {
    $handle = $mysql->prepare('SELECT COUNT(*) AS "count" FROM t_benutzer_gruppen WHERE PF_Email = ? AND PF_Gruppe = ?');

    $handle->bindValue(1, $email);
    $handle->bindValue(2, $gruppe);

    if($handle->execute()) {
        if($handle->fetch(PDO::FETCH_OBJ)->count == "1") {
            return true;
        }
    }
    return false;
}

function getGruppe($mysql, $gruppe) {
    $handle = $mysql->prepare('SELECT DISTINCT * FROM `t_gruppen` WHERE P_Gruppe = ?');

    $handle->bindValue(1, $gruppe);

    if($handle->execute()) {
        return $handle->fetch(PDO::FETCH_OBJ);
    }
    return false;
}

function getGruppenmitglieder($mysql, $gruppe) {
    $handle = $mysql->prepare('SELECT DISTINCT * FROM `t_benutzer_gruppen` WHERE PF_Gruppe = ?');

    $handle->bindValue(1, $gruppe);

    if($handle->execute()) {
        return $handle->fetchAll(PDO::FETCH_OBJ);
    }
    return false;
}

function insertGruppeKlasse($mysql, $klasse, $gruppe) {
    $handle = $mysql->prepare('INSERT INTO `t_klassen_gruppen` (`PF_Klasse`, `PF_Gruppe`) VALUES (?, ?);');
 
    $handle->bindValue(1, $klasse);
    $handle->bindValue(2, $gruppe);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

function getGruppenByKlasse($mysql, $klasse) {
    $handle = $mysql->prepare('SELECT * FROM `t_klassen_gruppen` kg JOIN t_benutzer_gruppen bg ON bg.PF_Gruppe = kg.PF_Gruppe JOIN t_gruppen g ON g.P_Gruppe = kg.PF_Gruppe WHERE PF_Klasse = ? GROUP BY bg.PF_Gruppe');

    $handle->bindValue(1, $klasse);

    if($handle->execute()) {
        return $handle->fetchAll(PDO::FETCH_OBJ);
    }
    return false;
}

function Gruppenüberweisung($mysql, $betrag, $gruppe) {
    $handle = $mysql->prepare('UPDATE `t_gruppen` SET `Gruppenkasse` = Gruppenkasse + ? WHERE `t_gruppen`.`P_Gruppe` = ?;');
 
    $handle->bindValue(1, $betrag);
    $handle->bindValue(2, $gruppe);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

function GruppeVerlassen($mysql, $email, $gruppe) {
    $handle = $mysql->prepare('DELETE FROM `t_benutzer_gruppen` WHERE PF_Email = ? AND PF_Gruppe = ?');
 
    $handle->bindValue(1, $email);
    $handle->bindValue(2, $gruppe);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

function updateCode($mysql, $email, $code) {
    $handle = $mysql->prepare('UPDATE `t_benutzer` SET `Code` = ? WHERE `t_benutzer`.`P_Email` = ?;');
 
    $handle->bindValue(1, $code);
    $handle->bindValue(2, $email);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

function insertPromocode($mysql, $promocode, $wert) {
    $handle = $mysql->prepare('INSERT INTO `t_promocodes` (`P_Promocode`, `Wert`) VALUES (?, ?);');
 
    $handle->bindValue(1, $promocode);
    $handle->bindValue(2, $wert);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

function getPromocodes($mysql) {
    $handle = $mysql->prepare('SELECT * FROM `t_promocodes`');


    if($handle->execute()) {
        return $handle->fetchAll(PDO::FETCH_OBJ);
    }
    return false;
}

function getPromocode($mysql, $promocode) {
    $handle = $mysql->prepare('SELECT * FROM `t_promocodes` WHERE P_Promocode = ?');

    $handle->bindValue(1, $promocode);

    if($handle->execute()) {
        try {
            $return = $handle->fetch(PDO::FETCH_OBJ);
            if(is_object($return)) {
                return $return;
            }
        } catch (\Throwable $th) {
            return false;
        }
    }
    return false;
}

function deletePromocode($mysql, $promocode) {
    $handle = $mysql->prepare('DELETE FROM `t_promocodes` WHERE P_Promocode = ?');
 
    $handle->bindValue(1, $promocode);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}

function getÜberweisung($mysql, $email, $zweck, $zeitpunkt) { 
    $handle = $mysql->prepare('SELECT * FROM `t_benutzer_überweisungen` WHERE `t_benutzer_überweisungen`.`PF_Email` = ? AND `t_benutzer_überweisungen`.`PF_Verwendungszweck` = ? AND `t_benutzer_überweisungen`.`Zeitpunkt` = ?;');
 
    $handle->bindValue(1, $email);
    $handle->bindValue(2, $zweck);
    $handle->bindValue(3, $zeitpunkt);


    if($handle->execute()) {
        return $handle->fetch(PDO::FETCH_OBJ);
    }
    return false;
}

function editÜberweisung($mysql, $neuerbetrag, $email, $zweck, $zeitpunkt) {
    $handle = $mysql->prepare('UPDATE `t_benutzer_überweisungen` SET `Betrag` = ? WHERE `t_benutzer_überweisungen`.`PF_Email` = ? AND `t_benutzer_überweisungen`.`PF_Verwendungszweck` = ? AND `t_benutzer_überweisungen`.`Zeitpunkt` = ?;');
 
    $handle->bindValue(1, $neuerbetrag);
    $handle->bindValue(2, $email);
    $handle->bindValue(3, $zweck);
    $handle->bindValue(4, $zeitpunkt);

    try {
        if($handle->execute()) {
            return true;
        }
    } catch (\Throwable $th) {
        return false;
    }
    return false;
}
