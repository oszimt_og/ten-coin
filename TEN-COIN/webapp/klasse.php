<?php
session_start();

require_once("model.php");
require_once("config.php");
require_once("functions.php");

$logged_in = False;
$is_lehrer = False;

if(isset($_SESSION["user_email"], $_GET["k"])) {
    $logged_in = True;
    $user_data = getUserByEmail($mysql, $_SESSION["user_email"]);
    $is_lehrer = isLehrer($user_data->P_Email);
}

$klasse = getUserByKlasse($mysql, $_GET["k"]);

if(!$is_lehrer || !$klasse) {
    header("Location: /");
    die();
} 

$success = False;

if(isset($_POST["gruppen_betrag"], $_GET["g"])) {
    if(Gruppenüberweisung($mysql, $_POST["gruppen_betrag"], $_GET["g"])) {
        $success = "Transkation wurde erfolgreich durchgeführt.";
    };
}

?>

<html>
    <head>
        <!-- CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <title>TenCoin - Lehreransicht</title>
        
    </head>

    <body>

         <div class="container">

            <br>

            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="/">TenCoin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home
                            <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <?php if($logged_in): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Abmelden</a>
                        </li>
                        <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Anmelden</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Registrieren</a>
                        </li>
                        <?php endif; ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/datenschutz">Datenschutzbestimmungen</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/impressum">Impressum</a>
                        </li>
                    </ul>

                </div>
            </nav>
            
            <br>

        

            <div class="jumbotron">
                <h1 class="display-3">Hallo,  <?= $user_data->Vorname  ?> <?= $user_data->Name  ?>!</h1>
                <p class="lead">Hier können Sie Ihre Schüler quälen und ausbeuten.</p>
                <hr class="my-4">
                <button class="btn btn-success" onclick="window.history.go(-1)">Zurück zum Start</button>
                <hr>
                <?php if(isset($_GET["g"])): ?>
                <h2>Mitglieder</h2>

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Vorname</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach (getGruppenmitglieder($mysql, $_GET["g"]) as $key => $mitglied): $mitglied_data = getUserByEmail($mysql,  $mitglied->PF_Email); ?>
                        <tr>
                        <td><?= $mitglied_data->Vorname ?></td>
                        <td><?= $mitglied_data->Name ?></td>
                        <td><?= $mitglied->PF_Email ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <hr>
                
                <h2>Transaktion mit <b><?= $_GET["g"] ?></b></h2>
                <?php if($success): ?>
                <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?= $success ?>
                </div> 
                <?php endif; ?>
                <form action="/klasse?k=<?=$_GET["k"]?>&g=<?=$_GET["g"]?>" method="post">
                    <fieldset>


                        <div class="form-group">
                            <div class="form-group">
                                <label>Betrag eingeben</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">$</span>
                                    </div>
                                    <input required name="gruppen_betrag" type="number" class="form-control" aria-label="Amount (to the nearest dollar)">
                                    <div class="input-group-append">
                                        <span class="input-group-text">.00</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success">Transaktion durchführen</button>
                    </fieldset>
                </form>    



                <?php else: ?>

                <h2>Ihre Schüler der <?= $_GET["k"] ?></h2>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Vorname</th>
                            <th scope="col">Nachname</th>
                            <th scope="col">Email</th>
                            <th scope="col">Kontostand</th>
                            <th scope="col">Notenpunkte</th>
                            <th scope="col">Verifikation</th>
                            <th scope="col">Aktionen</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($klasse as $key => $schueler): ?>
                        <?php if(!isLehrer($schueler->P_Email)): ?>
                        <tr>
                        <th scope="row"><?= $schueler->Vorname ?></th>
                        <td><?= $schueler->Name ?></td>
                        <td><?= $schueler->P_Email ?></td>
                        <td><?= $schueler->Total ?>$</td>
                        <td><?= dollarToNotenpunkt($notenpunkte, getTotalOfUser($mysql, $schueler->P_Email)) ?> NP</td>
                        <td><?= isVerifiziert($schueler->Code) ?></td>
                        <td><a href="/ueberweisung?s=<?= $schueler->P_Email ?>" type="button" class="btn btn-success">Überweisung</a> </td>
                        </tr>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <hr class="my-4">
                <h2>Gruppen der <?= $_GET["k"] ?></h2>
                
                <?php foreach (getGruppenByKlasse($mysql, $_GET["k"]) as $key => $gruppe): ?>
                    <a href="/klasse?k=<?=$_GET["k"]?>&g=<?= $gruppe->PF_Gruppe ?>" type="button" class="btn btn-outline-primary"><?= $gruppe->PF_Gruppe ?> - <?= $gruppe->Gruppenkasse ?>$</a>
                <?php endforeach; ?>

                <?php endif; ?>
            </div>



            <?php require_once("footer.php"); ?>

        </div>

        <!-- jQuery and JS bundle w/ Popper.js -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>