<?php
session_start();

require_once("model.php");
require_once("config.php");
require_once("functions.php");

$logged_in = False;
$is_lehrer = False;

if(isset($_SESSION["user_email"])) {
    $logged_in = True;
    $user_data = getUserByEmail($mysql, $_SESSION["user_email"]);
    $total_dollars = getTotalOfUser($mysql, $user_data->P_Email);
    $max_progress = calculatePercent($notenpunkte[14], $total_dollars);
    $next_notenpunkt = getNextNotepunkt($notenpunkte, $total_dollars);
    $next_progress = calculatePercent($next_notenpunkt, $total_dollars);
    $next_safe = calculatePercent($notenpunkte[4], $total_dollars);

    $is_lehrer = isLehrer($user_data->P_Email);
}


if(isset($_POST["klasse"]) && $is_lehrer) {
    $klasse = htmlentities($_POST["klasse"], ENT_QUOTES, 'UTF-8');
    insertKlasse($mysql, $klasse);
    insertUserKlasse($mysql, $user_data->P_Email, $klasse);
}

if(!$is_lehrer && $logged_in && $user_data->Code != 0) {
    header("Location: /verify");
    die();
}

if(isset($_POST["zweck"], $_POST["zeitpunkt"], $_POST["promocode"])) {
    $promocode_type = getPromocode($mysql, $_POST["promocode"]);
    $alter_betrag = getÜberweisung($mysql, $user_data->P_Email, $_POST["zweck"], $_POST["zeitpunkt"])->Betrag;

    if($promocode_type) {
        if($promocode_type->Wert == 2) {
            $neuer_betrag = $alter_betrag * 2;
            if(editÜberweisung($mysql, $neuer_betrag, $user_data->P_Email, $_POST["zweck"], $_POST["zeitpunkt"])) {
                deletePromocode($mysql, $_POST["promocode"]);
                header("Location: /");
            }
        }
        elseif($promocode_type->Wert == 5) {
            $neuer_betrag = $alter_betrag + 5;
            if(editÜberweisung($mysql, $neuer_betrag, $user_data->P_Email, $_POST["zweck"], $_POST["zeitpunkt"])) {
                deletePromocode($mysql, $_POST["promocode"]);
                header("Location: /");
            }
        }
    }
}

?>

<html>
    <head>
        <!-- CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <title>TenCoin - Home</title>
        
    </head>

    <body>

         <div class="container">

            <?php if(!$is_lehrer): ?>
             
            <br>

            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="/">TenCoin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home
                            <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <?php if($logged_in): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/gluecksspiel">Glücksspiel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/gruppen">Gruppen</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Abmelden</a>
                        </li>
                        <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Anmelden</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Registrieren</a>
                        </li>
                        <?php endif; ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/datenschutz">Datenschutzerklärung</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/impressum">Impressum</a>
                        </li>
                    </ul>

                </div>
            </nav>

            <br>
        


            <div class="jumbotron">
                <?php if($logged_in): ?>
                <h1 class="display-3">Hey, <?= $user_data->Vorname  ?>!</h1>
                <hr class="my-4">
                <h3>Du hast derzeit <b><?= $total_dollars ?>$</b> von erreichbaren <b><?= $notenpunkte[14] ?>$</b>.</h3>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: <?= $max_progress ?>%;" aria-valuenow="<?= $max_progress ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <br>
                <h3>Dir fehlen noch <b><?= differenz($next_notenpunkt, $total_dollars) ?>$</b> um <b><?= dollarToNotenpunkt($notenpunkte, $total_dollars) ?>NP</b> zu erreichen.</h3>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: <?= $next_progress ?>%;" aria-valuenow="<?= $next_progress ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <br>
                <h3>Um zu bestehen und <b>5 NP</b> zu erreichen, benötigst du noch <b><?= differenz($notenpunkte[4], $total_dollars) ?>$</b>.</h3>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: <?= $next_safe ?>%;" aria-valuenow="<?= $next_safe ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <hr class="my-4">
                <h2>Transaktionen</h2>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Zweck</th>
                            <th scope="col">Zeitpunkt</th>
                            <th scope="col">Betrag</th>
                            <th scope="col">Aktion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach (getUeberweisungenByUser($mysql, $user_data->P_Email) as $key => $ueberweisung): ?>
                        <?php if($ueberweisung->Betrag != 0): ?>
                        <form action="index.php" method="post">
                        <tr class="<?=getTable($ueberweisung->Betrag)?>">
                        <th scope="row"><?= $ueberweisung->PF_Verwendungszweck ?></th>
                        <input style="display: none;" name="zweck" value="<?= $ueberweisung->PF_Verwendungszweck ?>">
                        <td><?= $ueberweisung->Zeitpunkt ?></td>
                        <input style="display: none;" name="zeitpunkt" value="<?= $ueberweisung->Zeitpunkt ?>">
                        <td><b><?= $ueberweisung->Betrag ?>$</b></td>
                        <td>
                            <div class="form-group">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input required placeholder="Promocode" min="10" name="promocode" type="number" class="form-control">
                                        <div class="input-group-append">
                                        <button type="submit" class="btn btn-success">Anwenden</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        </form>
                        </tr>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php else: ?>
                <h1 class="display-3">Willkommen, OSZ IMT!</h1>
                <p class="lead">Diese Seite wird als dein persönliches IT-Dollar Wallet dienen. Hier kannst du IT-Dollar von deinem Lehrer erhalten und vieles weiteres tun.</p>
                <hr class="my-4">
                <p>Melde dich jetzt an oder registriere dich.</p>
                <p class="lead">
                    <a class="btn btn-primary btn-lg" href="/register" role="button">Registrieren</a>
                </p>
                <?php endif; ?>
            </div>

            <?php else: ?>

            <br>

            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="/">TenCoin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home
                            <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <?php if($logged_in): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Abmelden</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/promocodes">Promocodes</a>
                        </li>
                        <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Anmelden</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Registrieren</a>
                        </li>
                        <?php endif; ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/datenschutz">Datenschutzerklärung</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/impressum">Impressum</a>
                        </li>
                    </ul>

                </div>
            </nav>
            
            <br>

            <div class="jumbotron">
                <h1 class="display-3">Hallo, <?= $user_data->Vorname  ?> <?= $user_data->Name  ?>!</h1>
                <p class="lead">Hier können Sie Ihre Schüler quälen und ausbeuten.</p>
                <hr class="my-4">
                <h2>Ihre Klassen</h2>
                
                <?php foreach (getKlassenByLehrer($mysql, $user_data->P_Email) as $key => $klasse): ?>
                    <a href="/klasse?k=<?= $klasse->PF_Klasse ?>" type="button" class="btn btn-outline-primary"><?= $klasse->PF_Klasse ?></a>
                <?php endforeach; ?>

                <hr class="my-4">

                <h2>Neue Klasse erstellen</h2>

                <form action="index.php" method="post">
                    <fieldset>
                        <div class="form-group">
                            <label>Neue Klasse</label>
                            <input required name="klasse" type="text" class="form-control" placeholder="Klasse">
                        </div>

                        <button type="submit" class="btn btn-primary">Erstellen</button>
                    </fieldset>
                </form>

            </div>

            <?php endif; ?>

            <?php require_once("footer.php"); ?>

        </div>

        <!-- jQuery and JS bundle w/ Popper.js -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>