<?php

    session_start();

    if(isset($_SESSION["user_email"])) {
        header("Location: /");
        die();
    }

    require_once("model.php");

    $error = False;

    if(isset($_POST["email"], $_POST["pass"])):
        $email = htmlentities($_POST["email"], ENT_QUOTES, 'UTF-8');
        $pass = htmlentities($_POST["pass"], ENT_QUOTES, 'UTF-8');
        $user = getUserByEmail($mysql, $email);

        if(!$user):
            $error = "Ein Account mit dieser Email-Adresse existiert nicht.";
        elseif(!password_verify($pass, $user->Passwort)):
            $error = "Das eingegebene Passwort ist falsch.";
        else:
            $_SESSION["user_email"] = $email;
            header("Location: /");
            die();
        endif;

    endif;
?>

<html>
    <head>
        <!-- CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <title>TenCoin - Anmeldung</title>
        
    </head>

    <body>

         <div class="container">
             
            <br>

            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="/">TenCoin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home
                            <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Anmelden</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Registrieren</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/datenschutz">Datenschutzerklärung</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/impressum">Impressum</a>
                        </li>
                    </ul>

                </div>
            </nav>

            <br>

            <h1>Anmeldung</h1>

            <?php if($error): ?>
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?= $error ?>
                </div>
            <?php endif; ?>
        
            <form action="login.php" method="post">
                <fieldset>


                    <div class="form-group">
                        <label>Email</label>
                        <input required name="email"  type="email" class="form-control" placeholder="Email">
                    </div>

                    <div class="form-group">
                        <label>Passwort</label>
                        <input required name="pass" type="password" class="form-control" placeholder="Passwort">
                    </div>

                    <button type="submit" class="btn btn-primary">Anmelden</button>
                </fieldset>
            </form>

            <?php require_once("footer.php"); ?>

        </div>

        <!-- jQuery and JS bundle w/ Popper.js -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>