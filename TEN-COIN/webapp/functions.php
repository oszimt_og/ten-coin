<?php

function calculatePercent($max, $current) {
    return (100 * $current) / $max;
}

function getNextNotepunkt($notenpunkte, $current) {
    foreach ($notenpunkte as $key => $notenpunkt) {
        if($current < $notenpunkt) {
            return $notenpunkt;
        }
    }
}

function dollarToNotenpunkt($notenpunkte, $current) {
    if($current == 0) {
        return 0;
    }
    foreach ($notenpunkte as $key => $notenpunkt) {
        if($current < $notenpunkt) {
            return $key+1;
        }
    }
}

function differenz($a, $b) {
    if($b > $a) {
        return 0;
    }
    return $a - $b;
}

function isLehrer($email) {
    $domain = substr(strrchr($email, "@"), 1);
    if($domain == "oszimt.de") {
        return true;
    }
    return false;
}

function getTable($betrag) {
    if($betrag > 0) {
        return "table-success";
    }
    return "table-danger";
}

function generateCode() {
    return rand(10000, 99999);
}

function isVerifiziert($code) {
    if($code == 0) {
        return "verifiziert";
    }
    return $code;
}

function getUserIp() {
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        return $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    return $_SERVER['REMOTE_ADDR'];
}

function verifyRecaptcha($response, $ip) {
    $post_data = http_build_query(
        array(
            'secret' => "6Lf9M-4ZAAAAABg5xJbKbYka7trh1LRc3eCuauod",
            'response' => $response,
            'remoteip' => $ip
        )
    );
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $post_data
        )
    );
    $context  = stream_context_create($opts);
    $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    $result = json_decode($response);
    if ($result->success) {
        return true;
    }
    return false;
}

function generatePromocode() {
    return generateCode().generateCode();
}
?>