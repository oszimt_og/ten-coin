<?php
session_start();

require_once("model.php");
require_once("config.php");
require_once("functions.php");

$logged_in = False;
$is_lehrer = False;

if(isset($_SESSION["user_email"])) {
    $logged_in = True;
    $user_data = getUserByEmail($mysql, $_SESSION["user_email"]);
    $is_lehrer = isLehrer($user_data->P_Email);
}


if(!$is_lehrer) {
    header("Location: /");
    die();
} 

$success = False;

if(isset($_POST["typ"])) {
    if(insertPromocode($mysql, generatePromocode(), $_POST["typ"])) {
        $success = "Promocode wurde erfolgreich erstellt.";
    }
}


?>

<html>
    <head>
        <!-- CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <title>TenCoin - Lehreransicht</title>
        
    </head>

    <body>

         <div class="container">

            <br>

            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="/">TenCoin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home
                            <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <?php if($logged_in): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Abmelden</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/promocodes">Promocodes</a>
                        </li>
                        <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Anmelden</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Registrieren</a>
                        </li>
                        <?php endif; ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/datenschutz">Datenschutzbestimmungen</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/impressum">Impressum</a>
                        </li>
                    </ul>

                </div>
            </nav>
            
            <br>

        

            <div class="jumbotron">
                <h1 class="display-3">Hallo,  <?= $user_data->Vorname  ?> <?= $user_data->Name  ?>!</h1>
                <p class="lead">Hier können Sie Ihre Schüler quälen und ausbeuten.</p>
                <hr class="my-4">
                <h2>Promocodes erstellen</h2>

                <?php if($success): ?>
                <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?= $success ?>
                </div>
                <?php endif; ?>
                
                <form action="/promocodes" method="post">
                    <fieldset>

                      <div class="form-group">
                            <label for="exampleSelect1">Typ auswählen</label>
                            <select required name="typ" class="form-control" id="exampleSelect1">
                                <option value="2">Verdoppler</option>
                                <option value="5">Extra 5$</option>
                            </select>
                        </div>


                        <button type="submit" class="btn btn-success">Promocode erstellen</button>
                    </fieldset>
                </form>  
                
                <hr class="my-4">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Code</th>
                            <th scope="col">Typ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach (getPromocodes($mysql) as $key => $promocode): ?>
                        <tr>
                        <td><?= $promocode->P_Promocode ?></td>
                        <td><?php if($promocode->Wert == 2) echo "Doppler"; else echo "Extra 5$" ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>



            <?php require_once("footer.php"); ?>

        </div>

        <!-- jQuery and JS bundle w/ Popper.js -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>