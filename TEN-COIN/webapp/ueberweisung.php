<?php
session_start();

require_once("model.php");
require_once("config.php");
require_once("functions.php");

$logged_in = False;
$is_lehrer = False;

if(isset($_SESSION["user_email"], $_GET["s"])) {
    $logged_in = True;
    $user_data = getUserByEmail($mysql, $_SESSION["user_email"]);
    $is_lehrer = isLehrer($user_data->P_Email);
}

$schueler = getUserByEmail($mysql, $_GET["s"]);

if(!$is_lehrer || !$schueler) {
    header("Location: /");
    die();
} 


$success = False;
if(isset($_POST["betrag"], $_POST["zweck"], $_GET["s"])) {
    if(insertUeberweisung($mysql, $_GET["s"], $_POST["zweck"], $_POST["betrag"])) {
        $success = "Die Transaktion wurde erfolgreich verarbeitet.";
    }
}

if(isset($_POST["zweck_neu"])) {
    $zweck = htmlentities($_POST["zweck_neu"], ENT_QUOTES, 'UTF-8');
    if(insertVerwendungszweck($mysql, $zweck)) {
        $success = "Verwendungszweck '".$zweck."' wurde hinzugefügt.";
    }
}


?>

<html>
    <head>
        <!-- CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <title>TenCoin - Überweisung</title>
        
    </head>

    <body>

         <div class="container">

            <br>

            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="/">TenCoin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home
                            <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <?php if($logged_in): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Abmelden</a>
                        </li>
                        <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Anmelden</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Registrieren</a>
                        </li>
                        <?php endif; ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/datenschutz">Datenschutzbestimmungen</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/impressum">Impressum</a>
                        </li>
                    </ul>

                </div>
            </nav>
            
            <br>

            <div class="jumbotron">
                <h1 class="display-3">Hallo,  <?= $user_data->Vorname  ?> <?= $user_data->Name  ?>!</h1>
                <p class="lead">Hier können Sie Ihre Schüler quälen und ausbeuten.</p>
                <hr class="my-4">
                <button class="btn btn-success" onclick="window.history.go(-1)">Zurück zur Klassenansicht</button>
                <hr>
                <h2>Transaktion mit <?= $schueler->Vorname." ".$schueler->Name ?></h2>
                <?php if($success): ?>
                <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?= $success ?>
                </div>
                <?php endif; ?>

                <form action="/ueberweisung?s=<?=$_GET["s"]?>" method="post">
                    <fieldset>

                      <div class="form-group">
                            <label for="exampleSelect1">Verwendungszweck auswählen</label>
                            <select required name="zweck" class="form-control" id="exampleSelect1">
                                <?php foreach (getVerwendungszwecke($mysql) as $key => $zweck): ?>
                                <option value="<?= $zweck->P_Verwendungszweck ?>"><?= $zweck->P_Verwendungszweck ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label>Betrag eingeben</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">$</span>
                                    </div>
                                    <input required name="betrag" type="number" class="form-control" aria-label="Amount (to the nearest dollar)">
                                    <div class="input-group-append">
                                        <span class="input-group-text">.00</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success">Transaktion durchführen</button>
                    </fieldset>
                </form>     
                <hr>
                <h2>Verwendungszwecke hinzufügen</h2>
                <form action="/ueberweisung?s=<?=$_GET["s"]?>" method="post">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-form-label" for="inputDefault">Verwendungszweck eingeben</label>
                            <input name="zweck_neu" type="text" class="form-control" placeholder="Neuer Verwendungszweck">
                        </div>
                        <button type="submit" class="btn btn-success">Verwendungszweck hinzufügen</button>
                    </fieldset>
                </form>  
            </div>



            <?php require_once("footer.php"); ?>

        </div>

        <!-- jQuery and JS bundle w/ Popper.js -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>