<?php
session_start();

require_once("model.php");
require_once("config.php");
require_once("functions.php");

$logged_in = False;
$is_lehrer = False;

if(isset($_SESSION["user_email"])) {
    $logged_in = True;
    $user_data = getUserByEmail($mysql, $_SESSION["user_email"]);
    $is_lehrer = isLehrer($user_data->P_Email);
}


if($is_lehrer || !$logged_in) {
    header("Location: /");
    die();
}

$success = False;
$error = False;


if(isset($_POST["gruppe"])) {
    $gruppe = htmlentities($_POST["gruppe"], ENT_QUOTES, 'UTF-8');
    if(!ctype_alnum($gruppe)) {
        echo "Gruppenname darf nur Zahlen und Buchstaben enthalten.";
        die();
    }
    if(insertGruppe($mysql, $gruppe)){
        addUserToGruppe($mysql, $gruppe, $user_data->P_Email);
        insertGruppeKlasse($mysql, getKlasseOfUser($mysql, $user_data->P_Email), $gruppe);
        $success = "Gruppe wurde erstellt.";
    } else {
        $error = "Eine Gruppe mit diesem Namen existiert bereits oder der Name darf nicht verwendet werden.";
    }
}

$gruppe = False;
if(isset($_GET["g"])) {
    if(isUserInGruppe($mysql, $user_data->P_Email, $_GET["g"])) {
        $gruppe = $_GET["g"];
    }
}

if(isset($_POST["mitglied_email"]) && $gruppe) {
    if(addUserToGruppe($mysql, $gruppe, $_POST["mitglied_email"])) {
        $success = "Mitschüler wurde zur Gruppe hinzugefügt.";
    } else {
        $error = "Mitschüler existiert nicht oder ist schon in der Gruppe.";
    }
}

if(isset($_POST["auszahlungs_betrag"]) && $gruppe) {
    if(getGruppe($mysql, $gruppe)->Gruppenkasse >= $_POST["auszahlungs_betrag"] && $_POST["auszahlungs_betrag"] > 0) {
        Gruppenüberweisung($mysql, "-".$_POST["auszahlungs_betrag"], $gruppe);
        insertUeberweisung($mysql, $user_data->P_Email, "Gruppenauszahlung", $_POST["auszahlungs_betrag"]);
        $success = "Auszahlung war erfolgreich.";
    } else {
        $error = "Ein Fehler ist bei der Auszahlung aufgetreten.";
    }
}

if(isset($_GET["verlassen"], $_GET["g"])) {
    GruppeVerlassen($mysql, $user_data->P_Email, $gruppe);
    header("Location: /gruppen");
}

if($user_data->Code != 0) {
    header("Location: /verify");
    die();
}

?>

<html>
    <head>
        <!-- CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <title>TenCoin - Gruppen</title>
        
    </head>

    <body>

         <div class="container">

             
            <br>

            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="/">TenCoin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home
                            <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <?php if($logged_in): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/gluecksspiel">Glücksspiel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/gruppen">Gruppen</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Abmelden</a>
                        </li>
                        <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Anmelden</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Registrieren</a>
                        </li>
                        <?php endif; ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/datenschutz">Datenschutzerklärung</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/impressum">Impressum</a>
                        </li>
                    </ul>

                </div>
            </nav>

            <br>
        


            <div class="jumbotron">
                <h1 class="display-3">Hey, <?= $user_data->Vorname  ?>!</h1>
                <hr class="my-4">
                <h3>Hier kannst Du Gruppen erstellen und Gruppenauszahlungen managen. <br> Bitte spreche Dich mit Deinen Gruppenmitgliedern ab, bevor du Dir Geld auszahlst.</h3>
                <hr class="my-4">
                <?php if($success): ?>
                <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?= $success ?>
                </div>
                <?php endif; ?>
                <?php if($error): ?>
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?= $error ?>
                </div>
                <?php endif; ?>
                
                <?php if($gruppe): ?>

                <h2>Deine Gruppe: <b><?= $gruppe ?></b></h2>
                <h2>Gruppenkasse: <b><?= getGruppe($mysql, $gruppe)->Gruppenkasse ?>$</b></h2>

                <hr>
                
                <h2>Mitglieder</h2>

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Vorname</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach (getGruppenmitglieder($mysql, $gruppe) as $key => $mitglied): $mitglied_data = getUserByEmail($mysql,  $mitglied->PF_Email); ?>
                        <tr>
                        <td><?= $mitglied_data->Vorname ?></td>
                        <td><?= $mitglied_data->Name ?></td>
                        <td><?= $mitglied->PF_Email ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <hr>


                <form action="/gruppen?g=<?=$gruppe?>" method="post">
                    <fieldset>
                        <div class="form-group">
                            <label>Mitglied hinzufügen</label>
                            <input required name="mitglied_email" type="text" class="form-control" placeholder="Mitglieds-Email">
                        </div>

                        <button type="submit" class="btn btn-primary">Hinzufügen</button>
                    </fieldset>
                </form> 

                <hr>
                    
                <form action="/gruppen?g=<?=$gruppe?>" method="post">
                    <fieldset>


                        <div class="form-group">
                            <div class="form-group">
                                <label>Anteil auszahlen</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">$</span>
                                    </div>
                                    <input min="1" required name="auszahlungs_betrag" type="number" class="form-control" aria-label="Amount (to the nearest dollar)">
                                    <div class="input-group-append">
                                        <span class="input-group-text">.00</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success">Transaktion durchführen</button>
                    </fieldset>
                </form>  
                
                <hr>

                <a href="/gruppen?g=<?=$gruppe?>&verlassen" type="button" class="btn btn-danger">Gruppe verlassen</a>

                <?php else: ?>

                <form action="/gruppen" method="post">
                    <fieldset>
                        <div class="form-group">
                            <label>Neue Gruppe erstellen</label>
                            <input required name="gruppe" type="text" class="form-control" placeholder="Gruppenname">
                        </div>

                        <button type="submit" class="btn btn-primary">Erstellen</button>
                    </fieldset>
                </form>    
                
                <hr class="my-4">

                <h2>Deine Gruppen</h2>

                <?php foreach (getGruppenByUser($mysql, $user_data->P_Email) as $key => $gruppe): ?>
                    <a href="/gruppen?g=<?= $gruppe->PF_Gruppe ?>" type="button" class="btn btn-outline-primary"><?= $gruppe->PF_Gruppe ?></a>
                <?php endforeach; ?>

                <?php endif; ?>


            </div>



            <?php require_once("footer.php"); ?>

        </div>

        <!-- jQuery and JS bundle w/ Popper.js -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>